package com.gitlab.mko575.sosl.parser;

import static com.gitlab.mko575.sosl.TestLogger.TEST_LOGGER;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.gitlab.mko575.sosl.AST.sos.OntologySeed;
import com.gitlab.mko575.sosl.parser.SOSLParser.SosFileContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.UserDefinedIdContext;
import com.gitlab.mko575.sosl.parser.TestHelper.TestVisitor;
import com.gitlab.mko575.sosl.parser.TestHelper.TestWalkListener;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.NoViableAltException;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

// @TestMethodOrder(MethodOrderer.Alphanumeric.class)
// @DisplayNameGeneration(ResourcesParsingTests.MyDNG.class)
class ResourcesParsingTests {

  /**
   * Test failing parsing unparsable SOSL unit test file.
   *
   * @param test the test to run
   */
  // @DisplayName("displayName")
  @ParameterizedTest(
  // name = "[{index,choice,9#0{index}|99#{index}}] {argumentsWithNames}"
  // name = "[{index}] {argumentsWithNames}"
  // name = "{arguments}"
  )
  @MethodSource("retrieveParsableSOSLTestFileNames")
  void testSuccessfullyParsingFile(ParseTestFromFile test) {
    TEST_LOGGER.logInfo("This test will try to parse: " + test);
    TEST_LOGGER.incrementIndentation();
    TEST_LOGGER.logInfo("test suite root: " + test.getTestSuiteRoot());
    TEST_LOGGER.logInfo("test file: " + test.getTestFilePath());
    try {
      SOSLParser parser =
          assertDoesNotThrow(() -> ParsingUtils.getParserOfFile(test.getTestFilePath()));
      ParsingUtils.setParserToFailFastMode(parser);
      SosFileContext ctx; // = null;
      try {
        ctx = parser.sosFile();
      } catch (ParseCancellationException e1) {
        TEST_LOGGER.logInfo("SLL mode can't do it, switching to LL mode.");
        parser = assertDoesNotThrow(() -> ParsingUtils.getParserOfFile(test.getTestFilePath()));
        ParsingUtils.setParserToTryHardMode(parser);
        parser.removeErrorListeners();
        parser.addErrorListener(new TestErrorListener());
        try {
          ctx = parser.sosFile();
        } catch (NullPointerException e2) {
          TEST_LOGGER.logInfo("Bumped into the ErrorListener exception. Trying again without them");
          // For an unknown reason the following 2 lines from above created an exception on file
          // symbols.sosl at org.antlr.v4.runtime.Parser.notifyErrorListeners(Parser.java:540).
          // parser.removeErrorListeners();
          // parser.addErrorListener(new TestErrorListener());
          parser = assertDoesNotThrow(() -> ParsingUtils.getParserOfFile(test.getTestFilePath()));
          ParsingUtils.setParserToTryHardMode(parser);
          ctx = parser.sosFile();
        }
      }
      TestHelper.assertDefaultSuccessFrom(ctx);
      TestHelper.assertDefaultSuccessFrom(test.toString(), parser, true);
      // Walking the result
      System.out.println("\nWalking through 'parsed tree':");
      TestWalkListener walkListener = new TestWalkListener();
      ParseTreeWalker.DEFAULT.walk(walkListener, ctx);
      // Visiting the result
      System.out.println("\nVisiting the 'parsed tree':");
      TestVisitor visitor = new TestVisitor();
      visitor.visit(ctx);
      // Testing CST2AST
      Map<String, OntologySeed> res = (new CST2ASTVisitor()).visit(ctx);
      Collection<OntologySeed> sosSet = res.values();
      assertEquals(ctx.ontologyDefinition().size(), sosSet.size());
      ctx.ontologyDefinition()
          .forEach(
              od -> {
                assertTrue(sosSet.stream().anyMatch(os -> os.id().equals(od.ontologyId.getText())));
                TestHelper.flattenPTree(od)
                    .filter(pt -> pt instanceof UserDefinedIdContext)
                    .map(pt -> ((UserDefinedIdContext) pt).id.getText())
                    .forEach(
                        id ->
                            assertTrue(
                                sosSet.stream()
                                    .anyMatch(
                                        os ->
                                            os.id().equals(od.ontologyId.getText())
                                                && os.sets().contains(id)),
                                String.format("'%s' has not been converted", id)));
              });
      // End test
      TEST_LOGGER.logInfo("Test OK", true);
    } finally {
      TEST_LOGGER.decrementIndentation();
      TEST_LOGGER.logInfo("");
    }
  }

  /**
   * Test failing parsing unparsable SOSL unit test file.
   *
   * @param test the test to run
   */
  @ParameterizedTest()
  @MethodSource("retrieveUnparsableSOSLUnitTestFileNames")
  void testFailingParsingFile(ParseTestFromFile test) {
    TEST_LOGGER.logInfo("This test will parse: " + test);
    TEST_LOGGER.incrementIndentation();
    TEST_LOGGER.logInfo("test suite root: " + test.getTestSuiteRoot());
    TEST_LOGGER.logInfo("test file: " + test.getTestFilePath());
    try {
      SOSLParser parser =
          assertDoesNotThrow(() -> ParsingUtils.getParserOfFile(test.getTestFilePath()));
      ParsingUtils.setParserToFailFastMode(parser);
      ParserRuleContext ctx = null;
      try {
        ctx = parser.sosFile();
      } catch (ParseCancellationException e) {
        TEST_LOGGER.logInfo("SLL mode can't do it, switching to LL mode.");
        parser = assertDoesNotThrow(() -> ParsingUtils.getParserOfFile(test.getTestFilePath()));
        ParsingUtils.setParserToTryHardMode(parser);
        parser.removeErrorListeners();
        parser.addErrorListener(new TestErrorListener());
        try {
          ctx = parser.sosFile();
        } catch (NoViableAltException nvaE) {
          System.out.println(nvaE + ": " + nvaE.getMessage());
        }
      }
      assertTrue(parser.getNumberOfSyntaxErrors() > 0);
      if (ctx != null) {
        // Walking the result
        System.out.println("\nWalking through 'parsed tree':");
        TestWalkListener walkListener = new TestWalkListener();
        ParseTreeWalker.DEFAULT.walk(walkListener, ctx);
        // Visiting the result
        System.out.println("\nVisiting the 'parsed tree':");
        TestVisitor visitor = new TestVisitor();
        visitor.visit(ctx);
      }
      TEST_LOGGER.logInfo("Test OK", true);
    } finally {
      TEST_LOGGER.decrementIndentation();
      TEST_LOGGER.logInfo("");
    }
  }

  /**
   * Returns the collection of parsable SOSL (unit and integration) test file paths.
   *
   * @return the collection of parsable SOSL (unit and integration) test file paths.
   */
  private static List<ParseTestFromFile> retrieveParsableSOSLTestFileNames() {
    List<ParseTestFromFile> allParsableTests = retrieveParsableSOSLUnitTestFileNames();
    allParsableTests.addAll(retrieveParsableSOSLIntegrationTestFileNames());
    return allParsableTests;
  }

  /**
   * Returns the collection of parsable SOSL integration test file paths.
   *
   * @return the collection of parsable SOSL integration test file paths.
   */
  private static List<ParseTestFromFile> retrieveParsableSOSLIntegrationTestFileNames() {
    String rootPathStr = "./SOSL_files/integrationTestFiles";
    return retrieveSOSLFileNamesIn(rootPathStr);
  }

  /**
   * Returns the collection of parsable SOSL unit test file paths.
   *
   * @return the collection of parsable SOSL unit test file paths.
   */
  private static List<ParseTestFromFile> retrieveParsableSOSLUnitTestFileNames() {
    String rootPathStr = "./SOSL_files/unitTestsFiles/successTests";
    return retrieveSOSLFileNamesIn(rootPathStr);
  }

  /**
   * Returns the collection of unparsable SOSL unit test file paths.
   *
   * @return the collection of unparsable SOSL unit test file paths.
   */
  private static List<ParseTestFromFile> retrieveUnparsableSOSLUnitTestFileNames() {
    String rootPathStr = "./SOSL_files/unitTestsFiles/errorTests";
    return retrieveSOSLFileNamesIn(rootPathStr);
  }

  /**
   * Returns the collection of SOSL files in the test resource under the provided root.
   *
   * @param filesRoot the root path where to look for files.
   * @return the collection of SOSL file paths under the provided root.
   */
  private static List<ParseTestFromFile> retrieveSOSLFileNamesIn(String filesRoot) {
    URL rootURL = ClassLoader.getSystemResource(filesRoot);
    if (rootURL == null) {
      return new ArrayList<ParseTestFromFile>();
    }
    Path rootPath = Paths.get(rootURL.getPath());
    List<ParseTestFromFile> res; // = new ArrayList<ParseTestFromFile>();
    try {
      res =
          ParsingUtils.retrieveFilesInSystemResources(filesRoot, "glob:**.sosl")
              .map((Path p) -> new ParseTestFromFile(rootPath, p))
              .collect(Collectors.toList());
    } catch (IOException e) {
      String msg =
          String.format(
              "IOException generated while looking for SOSL files into '%1$s': %2$s",
              filesRoot, e.getMessage());
      System.err.println(msg);
      res = Arrays.asList(new ParseTestFromFile[0]); // new ArrayList<ParseTestFromFile>();
    }
    res.sort((t1, t2) -> t1.getTestFilePath().compareTo(t2.getTestFilePath()));
    return res;
  }

  /**
   * Internal class used to deal with {@code @ParameterizedTest} based on files.
   */
  private static class ParseTestFromFile {
    /** The root of all test files of the test suite this test belongs to. */
    private final transient Path testSuiteRootPath;

    /** The path of the file this test is based on. */
    private final Path testFilePath;

    /**
     * Default Constructor.
     *
     * @param r "root" of all test files.
     * @param p specific file used for this test (including the root path).
     */
    public ParseTestFromFile(Path r, Path p) {
      this.testSuiteRootPath = r;
      this.testFilePath = p;
    }

    /**
     * Returns the root of the test suite.
     *
     * @return the root of the test suite.
     */
    public Path getTestSuiteRoot() {
      return testSuiteRootPath;
    }

    /**
     * Returns the test file path.
     *
     * @return the test file path.
     */
    public Path getTestFilePath() {
      return testFilePath;
    }

    @Override
    public String toString() {
      return testSuiteRootPath.relativize(testFilePath).toString();
    }
  }

  /*
  static class MyDNG extends DisplayNameGenerator.ReplaceUnderscores {

      @Override
      public String generateDisplayNameForClass(Class<?> testClass) {
          return super.generateDisplayNameForClass(testClass);
      }

      @Override
      public String generateDisplayNameForNestedClass(Class<?> nestedClass) {
          return super.generateDisplayNameForNestedClass(nestedClass) + "...";
      }

      @Override
      public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
          String name = testMethod.getName();
          return name.replace('_', ' ') + '.';
      }

  }
  */
}
