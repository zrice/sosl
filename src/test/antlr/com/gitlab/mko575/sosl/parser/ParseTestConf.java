package com.gitlab.mko575.sosl.parser;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.antlr.v4.runtime.Lexer;

public class ParseTestConf {

  /** Number of instances created so far. */
  private static int nbInstances = 0;

  /** Prefix used when automatically generating test names. */
  private static String defaultNamePrefix = "";

  /** Unique ID assigned to the test. */
  private final transient int uniqueId;

  /**
   * The enumeration of test configuration keys that can be set in the configuration map.
   */
  public enum ConfKeys {
    /** The name of the test as assigned at creation thru the API. */
    NAME,
    /** The text to parse. */
    SOS_STR,
    /** The name of the non-terminal to test against. */
    NON_TERMINAL,
    /** The lexingMode to start in. */
    LEXING_MODE,
    /** If the parsing test should succeed or fail. */
    PARSABLE,
    /** The text parsed as CST. */
    SOS_CST,
    /** The text parsed as AST. */
    SOS_AST
  }

  /**
   * Test configuration data stored in a Map for extensibility of tests setup
   * as development goes on.
   */
  private final transient Map<ConfKeys, Object> configuration =
      new HashMap<>(Map.of(
        ConfKeys.LEXING_MODE, Lexer.DEFAULT_MODE,
        ConfKeys.PARSABLE, true
      ));

  /**
   * Basic constructor of a test configuration using a Map to store
   * configuration data in order to increase extensibility of tests setup as
   * development goes on.
   *
   * @param configuration The parameters for this test.
   */
  public ParseTestConf(Map<ConfKeys, Object> configuration) {
    nbInstances += 1;
    this.uniqueId = nbInstances;
    this.configuration.putAll(configuration);
  }

  private static final String ERRMSG_WRONG_CONF_TYPE =
      "Trying to configure %s with a value of type %s.";

  /**
   * Add a configuration parameter to the test if it is not {@code null}.
   * @param k The key identifying the configuration parameter.
   * @param v The value of the configuration parameter.
   * @return {@code true} iff {@code k} is non {@code null}.
   */
  public boolean addParameterIfNonNull(ConfKeys k, Object v) {
    if (v != null) {
      switch (k) {
        case NAME:
        case SOS_STR:
        case NON_TERMINAL:
          if (! (v instanceof String)) {
            fail(String.format(ERRMSG_WRONG_CONF_TYPE, k, v.getClass()));
          }
          break;
        case LEXING_MODE:
          if (! (v instanceof Integer)) {
            fail(String.format(ERRMSG_WRONG_CONF_TYPE, k, v.getClass()));
          }
          break;
        case PARSABLE:
          if (! (v instanceof Boolean)) {
            fail(String.format(ERRMSG_WRONG_CONF_TYPE, k, v.getClass()));
          }
          break;
        default:
          assertTrue(true); // Just to satisfy CheckStyle's MissingSwitchDefault rule.
      }
      configuration.put(k, v);
      return true;
    } else {
      return false;
    }
  }

  /**
   * Basic constructor of a test configuration.
   *
   * @param testName name of the test
   * @param ntName name of the non-terminal to test against.
   * @param lexingMode mode to start lexing in
   * @param text text to parse.
   * @param parsable {@code true} iff the text can be parsed as the non-terminal
   *     whose name is associated to {@link ConfKeys#NON_TERMINAL}.
   * @param asAST if not {@code null}, the AST the text should be parsed to.
   */
  public ParseTestConf(
      String testName, String ntName, int lexingMode, String text, boolean parsable, Object asAST
  ) {
    this(new HashMap<ConfKeys, Object>());
    this.addParameterIfNonNull(ConfKeys.NAME, testName);
    this.addParameterIfNonNull(ConfKeys.SOS_STR, text);
    this.addParameterIfNonNull(ConfKeys.NON_TERMINAL, ntName);
    this.addParameterIfNonNull(ConfKeys.LEXING_MODE, lexingMode);
    this.addParameterIfNonNull(ConfKeys.PARSABLE, parsable);
    this.addParameterIfNonNull(ConfKeys.SOS_AST, asAST);
  }

  /**
   * Constructor for a "parsable" test.
   *
   * @param testName name of the test
   * @param ntName name of the non-terminal to test against.
   * @param text text to parse.
   * @param parsable {@code true} iff the text can be parsed as the non-terminal
   *     whose name is associated to {@link ConfKeys#NON_TERMINAL}.
   * @param asAST expected AST result
   */
  public ParseTestConf(
      String testName, String ntName,
      String text, boolean parsable, Object asAST
  ) {
    this(testName, ntName, 0, text, parsable, asAST);
  }

  /**
   * Constructor for a "parsable" test.
   *
   * @param testName name of the test
   * @param ntName name of the non-terminal to test against.
   * @param text text to parse.
   * @param parsable {@code true} iff the text can be parsed as the non-terminal
   *     whose name is associated to {@link ConfKeys#NON_TERMINAL}.
   */
  public ParseTestConf(String testName, String ntName, String text, boolean parsable) {
    this(testName, ntName, 0, text, parsable, null);
  }

  /**
   * Constructor for a "parsable" test.
   *
   * @param testName name of the test
   * @param ntName name of the non-terminal to test against.
   * @param text text to parse.
   */
  public ParseTestConf(String testName, String ntName, String text) {
    this(testName, ntName, text, true);
  }

  /**
   * Constructor for a "parsable" test.
   *
   * @param testName name of the test
   * @param ntName name of the non-terminal to test against.
   * @param text text to parse.
   * @param asAST expected AST result
   */
  public ParseTestConf(String testName, String ntName, String text, Object asAST) {
    this(testName, ntName, text, true, asAST);
  }

  /**
   * Constructor for a "parsable" test.
   *
   * @param ntName name of the non-terminal to test against.
   * @param lexMode mode to start lexing in
   * @param text text to parse.
   */
  public ParseTestConf(String ntName, int lexMode, String text) {
    this(null, ntName, lexMode, text, true, null);
  }

  /**
   * Constructor for a "parsable" test.
   *
   * @param ntName name of the non-terminal to test against.
   * @param text text to parse.
   * @param asAST expected AST result
   */
  public ParseTestConf(String ntName, String text, Object asAST) {
    this(null, ntName, text, asAST);
  }

  /**
   * Constructor for a "parsable" test.
   *
   * @param ntName name of the non-terminal to test against.
   * @param text text to parse.
   */
  public ParseTestConf(String ntName, String text) {
    this(null, ntName, text);
  }

  /**
   * Constructor for a test.
   *
   * @param ntName name of the non-terminal to test against.
   * @param text text to parse.
   * @param parsable {@code true} iff the text can be parsed as the non-terminal
   *     whose name is associated to {@link ConfKeys#NON_TERMINAL}.
   */
  public ParseTestConf(String ntName, String text, boolean parsable) {
    this(null, ntName, text, parsable);
  }

  /**
   * Setter for the default name prefix to use when automatically generating test names ({@link
   * #defaultNamePrefix}).
   *
   * @param dfltNamePrefix default name prefix to use.
   */
  public static void setDefaultNamePrefix(String dfltNamePrefix) {
    defaultNamePrefix = dfltNamePrefix;
  }

  /**
   * Returns the default name prefix ({@link #defaultNamePrefix}).
   *
   * @return the default name prefix ({@link #defaultNamePrefix}).
   */
  public static String getDefaultNamePrefix() {
    return defaultNamePrefix;
  }

  /**
   * Returns a unique generated name for the test.
   *
   * @param prefix prefix to use to generate a unique name.
   * @return a unique generated name for the test.
   */
  public String getGeneratedUniqueName(String prefix) {
    return (prefix.isEmpty() ? "" : prefix + "_") + String.format("%1$03d", uniqueId);
  }

  /**
   * Returns a unique generated name for the test.
   *
   * @return a unique generated name for the test.
   */
  public String getGeneratedUniqueName() {
    return getGeneratedUniqueName(defaultNamePrefix);
  }

  /**
   * Check if a configuration data is available.
   *
   * @param ck The key of the test configuration data to check for.
   * @return {@code true} is .
   */
  public boolean isConfDataAvailable(ConfKeys ck) {
    return configuration.containsKey(ck) && (configuration.get(ck) != null);
  }

  /**
   * Getter for the configuration data whose key is provided as parameter.
   *
   * @param ck The key of the test configuration data to retrieve.
   * @return The configuration data asked, or {@code null} if this data has not been configured.
   */
  public Object getConfData(ConfKeys ck) {
    return configuration.get(ck);
  }

  /**
   * Getter for the configuration data associated to {@link ConfKeys#NAME}.
   *
   * @return the NT name associated to the test.
   */
  public String getTestName() {
    return Objects.requireNonNullElseGet(
        (String) getConfData(ConfKeys.NAME),
        this::getGeneratedUniqueName
    );
  }

  /**
   * Getter for the configuration data associated to {@link ConfKeys#NON_TERMINAL}.
   *
   * @return the NT name associated to the test.
   */
  public String getNtName() {
    Object o = getConfData(ConfKeys.NON_TERMINAL);
    return (o instanceof String) ? (String) o : null;
  }


  /**
   * Getter for the configuration data associated to {@link ConfKeys#LEXING_MODE}.
   *
   * @return the lexing mode to start lexing this test in.
   */
  public int getLexingMode() {
    Object o = getConfData(ConfKeys.LEXING_MODE);
    return (o instanceof Integer) ? (int) o
        : fail("A non integer value has been stored in the 'LEXING_MODE' configuration parameter.");
  }

  /**
   * Getter for the configuration data associated to {@link ConfKeys#SOS_STR}.
   *
   * @return the text to parse.
   */
  public String getText() {
    Object o = getConfData(ConfKeys.SOS_STR);
    return (o instanceof String) ? (String) o : null;
  }

  /**
   * Getter for the configuration data associated to {@link ConfKeys#PARSABLE}.
   *
   * @return {@code true} iff the test text can be parsed as the non-terminal
   *     whose name is associated to {@link ConfKeys#NON_TERMINAL}.
   */
  public boolean isParsable() {
    Object o = getConfData(ConfKeys.PARSABLE);
    return (o instanceof Boolean) ? (boolean) o
        : fail("A non boolean value has been stored in the 'PARSABLE' configuration parameter.");
  }

  /**
   * Getter for the data associated to {@link ConfKeys#SOS_CST}.
   *
   * @return a CST Object corresponding to {@link ConfKeys#SOS_STR} if it is parsable as such
   */
  public Object getResultingCST() {
    return getConfData(ConfKeys.SOS_CST);
  }

  /**
   * Getter for the data associated to {@link ConfKeys#SOS_AST}.
   *
   * @return an AST Object corresponding to {@link ConfKeys#SOS_STR} if it is parsable as such
   */
  public Object getResultingAST() {
    return getConfData(ConfKeys.SOS_AST);
  }

  @Override
  public String toString() {
    String prefix = getDefaultNamePrefix().isEmpty() ? "" : getDefaultNamePrefix() + "_";
    return getGeneratedUniqueName(prefix + getNtName());
  }
}
