package com.gitlab.mko575.sosl.parser;

import static com.gitlab.mko575.sosl.TestLogger.TEST_LOGGER;

import java.util.BitSet;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.DiagnosticErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

public class TestErrorListener extends DiagnosticErrorListener {

  @Override
  public void syntaxError(
      Recognizer<?, ?> recognizer,
      Object offendingSymbol,
      int line,
      int charPositionInLine,
      String msg,
      RecognitionException e
  ) {
    TEST_LOGGER.logInfo("Syntax error at (" + line + ":" + charPositionInLine + "): " + msg);
    if (offendingSymbol instanceof CommonToken) {
      CommonToken os = (CommonToken) offendingSymbol;
      TEST_LOGGER.logIndentedInfo(
          "Offending symbol: '" + os.getText() + "' of type " + os.getType()
      );
    } else {
      TEST_LOGGER.logIndentedInfo("Class of offending symbol: " + offendingSymbol.getClass());
    }
    if (e != null) {
      super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);
      TEST_LOGGER.logIndentedInfo("Exception: " + e.getClass() + " : " + e.getMessage());
      throw e;
    } else {
      TEST_LOGGER.logIndentedInfo("No exception");
      throw new RecognitionException(
          msg, recognizer, recognizer.getInputStream(), new ParserRuleContext());
    }
  }

  @Override
  public void reportAmbiguity(
      Parser recognizer,
      DFA dfa,
      int startIndex,
      int stopIndex,
      boolean exact,
      BitSet ambigAlts,
      ATNConfigSet configs) {
    TEST_LOGGER.logInfo("Ambiguity error: " + getDecisionDescription(recognizer, dfa));
    super.reportAmbiguity(recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs);
  }

  @Override
  public void reportAttemptingFullContext(
      Parser recognizer,
      DFA dfa,
      int startIndex,
      int stopIndex,
      BitSet conflictingAlts,
      ATNConfigSet configs) {
    TEST_LOGGER.logInfo(
        "AttemptingFullContext error: " + getDecisionDescription(recognizer, dfa)
    );
    super.reportAttemptingFullContext(
        recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs);
  }

  @Override
  public void reportContextSensitivity(
      Parser recognizer,
      DFA dfa,
      int startIndex,
      int stopIndex,
      int prediction,
      ATNConfigSet configs) {
    TEST_LOGGER
        .logInfo("ContextSensitivity error: " + getDecisionDescription(recognizer, dfa));
    super.reportContextSensitivity(recognizer, dfa, startIndex, stopIndex, prediction, configs);
  }
}
