/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.cli;

import java.io.File;
import java.util.Collection;
import picocli.AutoComplete.GenerateCompletion;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParameterException;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Spec;

/**
 * This class only handles the top level CLI (Command Line Interface) command using
 * picocli and delegates all logic to subcommands defined in the same package.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
@Command(
    name = "forestry",
    subcommands = {
        CheckCmd.class,
        CultivateCmd.class,
        HarvestCmd.class,
        CommandLine.HelpCommand.class,
        GenerateCompletion.class,
    },
    description = "Process SOS files according to the specified subcommand.",
    version = {
        "$project_name $project_version",
        "  Picocli: " + picocli.CommandLine.VERSION,
        "  JVM: \${java.version} (\${java.vendor} \${java.vm.name} \${java.vm.version})",
        "  OS: \${os.name} \${os.version} \${os.arch}"
    },
    mixinStandardHelpOptions = true,
    showEndOfOptionsDelimiterInUsageHelp = true,
    showAtFileInUsageHelp = true
)
public class ForestryCLI implements Runnable {
  @Spec CommandSpec spec;

  /*--------------------------------------*/
  /* CLI TOP LEVEL OPTIONS AND PARAMETERS */
  /*--------------------------------------*/

  @Option(
      names = {"-v", "--verbose"},
      description = "Increase verbosity"
  )
  /**
   * Verbosity options entered on the top level part of the command line.
   */
  boolean[] verbosity;

  @Parameters(
      arity = "0..*",
      paramLabel = "<SOSL file>",
      description = "SOSL files to pass to subcommands"
  )
  /**
   * SOSL files to take into consideration for subcommands.
   */
  Collection<File> soslFiles;

  @Override
  public void run() {
    throw new ParameterException(spec.commandLine(), "Specify a subcommand");
  }

  /*-----------------*/
  /* PicoCLI tests   */
  /*-----------------*/

  // gradlew run --args="src/test/resources/SOSL_files/integrationTestFiles/empty.sosl pizza.sosl test p --option o --aOption a a2 --cOption c --mOption k=m --amOption k=am k2=am --cmOption k=cm --acmOption k=acm k2=acm --aOption a3 a --cOption c2 --mOption k2=m --amOption k3=am k=am2 --cmOption k2=cm --acmOption k3=acm k=acm2"

  /*
  @Command(name = "test", description = "Command to test PicoCLI features")
  void test(
      @Option(
              names = {"--option"},
              paramLabel = "option",
              description = "Simple option")
          String option,
      @Option(
              names = {"--aOption"},
              arity = "0..*",
              paramLabel = "aOption",
              description = "Arity option")
          Collection<String> aOption,
      @Option(
              names = {"--cOption"},
              paramLabel = "cOption",
              description = "Collection option (a.k.a. 'repeated option')")
          Collection<String> cOption,
      @Option(
              names = {"--mOption"},
              paramLabel = "mOption",
              description = "Map option")
          java.util.Map<Object,Object> mOption,
      @Option(
              names = {"--amOption"},
              arity = "0..*",
              paramLabel = "amOption",
              description = "Option")
          java.util.Map<Object,Object> amOption,
      @Option(
              names = {"--cmOption"},
              paramLabel = "cmOption",
              description = "Option")
          Collection<java.util.Map<Object,Object>> cmOption,
      @Option(
              names = {"--acmOption"},
              arity = "0..*",
              paramLabel = "acmOption",
              description = "Option")
          Collection<java.util.Map<Object,Object>> acmOption,
      @Parameters(
              paramLabel = "param",
              description = "Parameter")
          String param
  ) {
    System.out.println("soslFiles: " + soslFiles);
    System.out.println("option: " + option);
    System.out.println("aOption: " + aOption);
    System.out.println("cOption: " + cOption);
    System.out.println("mOption: " + mOption + ".any = " + mOption.entrySet().stream().findAny());
    System.out.println("mOption: " + mOption + ".size = " + mOption.size());
    System.out.println("amOption: " + amOption + ".any = " + amOption.entrySet().stream().findAny());
    System.out.println("amOption: " + amOption + ".size = " + amOption.size());
    System.out.println("cmOption: " + cmOption + ".any = " + cmOption.stream().findAny());
    System.out.println("cmOption: " + cmOption + ".size = " + cmOption.size());
    System.out.println("acmOption: " + acmOption + ".any = " + acmOption.stream().findAny());
    System.out.println("acmOption: " + acmOption + ".size = " + acmOption.size());
    System.out.println("param: " + param);
  }
   */
  
}
