/**
 * This is SOSL's grammar
 */

grammar SOSL;

import LexBasic, SOSLKeywords_asTokens;

/** LEXING RULES **/

fragment UC_Letter : [A-Z] ;
fragment LC_Letter : [a-z] ;
fragment Letter : ( UC_Letter | LC_Letter ) ;
fragment Digit : DecDigit ;
fragment EoL : ('\r'? '\n' | '\r') ;

fragment TQuote : '"""' ;

// Getting rid of unmeaningfull whitespaces
DISPOSABLE_SPACE : Ws+ -> channel(HIDDEN) ;

// Getting rid of semanticless comments
COMMENT : ( LineComment | BlockComment )+ -> channel(HIDDEN) ;

// Recognizing strings
STRING
    : SQuote ( ~["\r\n] | Esc DQuote )* SQuote
    | DQuote ( ~['\r\n] | Esc SQuote )* DQuote
    | TQuote ( . )*? TQuote
    ;

/** PARSING RULES **/

sosFile : ( ontologyDefinition | instanceDefinition )* EOF ;

/** ONTOLOGY DEFINITION PARSING **/

ontologyDefinition
  : ontologyId=WORD ontology_KW '{' body=ontologyDefBody '}'
  | begin_KW ontology_KW ontologyId=WORD body=ontologyDefBody end_KW ontology_KW?
  ;

ontologyDefBody : ( atomicFormula ';'? )* ;

/** FORMULA **/

atomicFormula
  : pred=unaryPred term                               #unaryPred_AF
  | lhs=term embeddedFormulae pred=binPred rhs=term   #binaryPred_AF
  | pred=naryPred term                                #naryPred_AF
  ;

embeddedFormulae: ( ( '+' | ':' ) pred=embedablePred )* ;

unaryPred
  : finite_KW | atomic_KW
  | functional_KW | total_KW | injective_KW | surjective_KW
  ;
binPred
  : subset_KW | subseteq_KW | superset_KW | superseteq_KW
  | in_KW | equal_KW
  ;
naryPred : distinct_KW ;

embedablePred : unaryPred | naryPred ;

/** TERM **/

term
  : setId                             #idConstant_ST
  | term ( ',' term )+                #list_ST
  | '{' ( term ( ',' term )* )? '}'   #set_ST
  | fun=unaryPrefixFun term           #unaryFunOnSet_ST
  | lhs=term fun=binFun rhs=term      #binaryFunOnSet_ST
  | '(' term ')'                      #closingTerm_ST
  ;

unaryPrefixFun
  : complement_prefix_KW
  | powerset_prefix_KW
  ;
binFun
  : union_KW | intersection_KW | cartesianProd_KW
  | partialFunction_KW   | totalFunction_KW     // X-to-1
  | partialInjection_KW  | totalInjection_KW    // 1-to-1
  | partialSurjection_KW | totalSurjection_KW   // onto
  | partialBijection_KW  | totalBijection_KW    // 1-to-1 and onto | 1-to-1 correspondence
  ;

setId
  : emptysetId_KW    #emptysetId
  | entitySetId_KW   #entitiesId
  | valueSetId_KW    #valuesId
  | userDefinedId    #userDefinedSetId
  ;

userDefinedId: id=WORD ;

/** INSTANCE DEFINITION PARSING **/

instanceDefinition
  : instanceId=WORD instance_KW ontologyId=WORD '{' instanceDefBody '}'
  | begin_KW instance_KW ontologyId=WORD instanceId=WORD body=instanceDefBody end_KW instance_KW?
  ;

instanceDefBody : ( atomicFormula ';'? )* ;
