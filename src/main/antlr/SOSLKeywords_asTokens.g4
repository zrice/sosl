
grammar SOSLKeywords_asTokens;

import LexBasic, SOSLWords;

/** PREDICATES **/

/*** on sets only ***/

FINITE : 'finite' ;
finite_KW : FINITE ;
ATOMIC : 'atomic' ;
atomic_KW : ATOMIC ;
DISTINCT : 'distinct' ;
distinct_KW : DISTINCT ;

SUBSET : '⊂' | 'subset' ( ' '* 'of' )? ;
subset_KW: SUBSET ;
SUBSETEQ : '⊆' | 'subset' ( 'eq' | ' '* 'of' ' '* 'or' ' '* 'equal' ' '* 'to') ;
subseteq_KW: SUBSETEQ ;
SUPERSET : '⊃' | 'superset' ( ' '* 'of' )? ;
superset_KW: SUPERSET ;
SUPERSETEQ : '⊇' | 'superset' ( 'eq' | ' '* 'of' ' '* 'or' ' '* 'equal' ' '* 'to') ;
superseteq_KW: SUPERSETEQ ;

/*** on relations only ***/

FUNCTIONAL : 'functional' ;
functional_KW : FUNCTIONAL ;
TOTAL : 'total' ;
total_KW : TOTAL ;
INJECTIVE : 'injective' ;
injective_KW : INJECTIVE ;
SURJECTIVE : 'surjective' ;
surjective_KW : SURJECTIVE ;

/*** other predicates ***/

IN : '∈' | 'in' | 'belongs' ' '* 'to' ;
in_KW: IN ; // ∉

EQUAL : '=' | 'equals' ( ' '* 'to' )? ;
equal_KW: EQUAL ; // ∉

/** FUNCTIONS **/

COMPLEMENT: 'C' | 'complement' ( 'Of' | ' '* 'of' ) ;
complement_prefix_KW: COMPLEMENT ;
POWERSET: 'P' | 'powerset' ( 'Of' | ' '* 'of' ) ;
powerset_prefix_KW: POWERSET ;

UNION: '∪' | 'union' ;
union_KW: UNION ;
INTERSECTION: '∩' | 'intersection' ;
intersection_KW: INTERSECTION ;
CARTESIAN_PRODUCT: '×' | '*' ;
cartesianProd_KW: CARTESIAN_PRODUCT ;

PARTIAL_FUNCTION: '⇸' | '-|->' ;
partialFunction_KW: PARTIAL_FUNCTION ;
TOTAL_FUNCTION: '→' | '-->' ;
totalFunction_KW: TOTAL_FUNCTION ;
PARTIAL_INJECTION: '⤔' | '>-|->' ;
partialInjection_KW: PARTIAL_INJECTION ;
TOTAL_INJECTION: '↣' | '>-->'  ;
totalInjection_KW: TOTAL_INJECTION ;
PARTIAL_SURJECTION: '⤀' | '-|->>' ;
partialSurjection_KW: PARTIAL_SURJECTION ;
TOTAL_SURJECTION: '↠' | '-->>' ;
totalSurjection_KW: TOTAL_SURJECTION ;
PARTIAL_BIJECTION: '⤗' | '>-|->>' ;
partialBijection_KW: PARTIAL_BIJECTION ;
TOTAL_BIJECTION: '⤖' | '>-->>' ;
totalBijection_KW: TOTAL_BIJECTION ;

/** CONSTANT SET IDENTIFIERS **/

ENTITY: 'Entity' ( 'Set' )? | 'Entities' ;
entitySetId_KW: ENTITY ;

VALUE: 'Value' ( 'Set' )? | 'Values' ;
valueSetId_KW: VALUE ;

EMPTYSET: '∅' | 'emptyset' ;
emptysetId_KW: EMPTYSET ;

/** KEYWORDS **/

BEGIN : ( 'BEGIN' | ('B'|'b') 'egin' ) ;
begin_KW : BEGIN ;
END : ( 'END' | ('E'|'e') 'nd') ;
end_KW : END ;

ONTOLOGY : ( 'ONTOLOGY' | ('O'|'o') 'ntology' ) ;
ontology_KW : ONTOLOGY ;

fragment Instance : ( 'INSTANCE' | ('I'|'i') 'nstance' ) ;
fragment Of : ( 'OF' | 'Of' | 'of' ) ;
INSTANCE : ( Instance ( ' '* Of )? | 'is' ' '* 'a' ) ;
instance_KW : INSTANCE ;


/** OTHER TOKENS **/

// Every token is a WORD, except those defined in parsing rules
WORD : Word ;

// Added in order to recognize every token to transform lexing errors into parsing errors
ERROR_CHAR : . ;
