/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.AST.gos

case class Arity(lowerBound:Int, upperBound:Option[Int]) {

  def toUMLString : String =
    (lowerBound, upperBound) match {
      case (0, None) => "*"
      case (lb, None) if 0 < lb => lb.toString + "..*"
      case (1, Some(1)) => ""
      case (lb, Some(ub)) if lb == ub => lb.toString
      case (lb, Some(ub)) if lb < ub => lb.toString + ".." + ub.toString
      case _ => "???"
    }

}

object Arity {
  val any : Arity = Arity(0, None)
  val optional : Arity = Arity(0, Some(1))
  val one : Arity = Arity(1, Some(1))
  val some : Arity = Arity(1, None)
}
