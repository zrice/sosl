/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.AST

import com.gitlab.mko575.sosl.AST.commons.EquippedSetTerm.equip

import scala.collection.Set

package object commons {
  type SetId = Object
}

package commons {
  sealed trait SetTerm

  sealed trait NamedSet extends SetTerm {
    val id: SetId
    val name: String
  }

  sealed trait AxiomaticSet extends NamedSet
  case object World extends AxiomaticSet {
    override val id: SetId = World
    override val name: String = "World"
  }
  case object Emptyset extends AxiomaticSet {
    override val id: SetId = Emptyset
    override val name: String = "Empty"
  }
  case object Singletons extends AxiomaticSet {
    override val id: SetId = Singletons
    override val name: String = "Singletons"
  }
  case object Entities extends AxiomaticSet {
    override val id: SetId = Entities
    override val name: String = "Entities"
  }
  case object Values extends AxiomaticSet {
    override val id: SetId = Values
    override val name: String = "Values"
  }
  object AxiomaticSets {
    val content : Set[AxiomaticSet] =
      Set[AxiomaticSet](World, Emptyset, Singletons, Entities, Values)

    def lub(a:AxiomaticSet, b:AxiomaticSet) : AxiomaticSet =
      (a,b) match {
        case (am, bm) if (am == bm) => am
        case (World, _) | (_, World) => World
        case (Singletons, _) | (_, Singletons) => Singletons
        case (Entities, Values) | (Values, Entities) => Singletons
        case (Emptyset, bm) => bm
        case (am, Emptyset) => am
      }

    def glb(a:AxiomaticSet, b:AxiomaticSet) : AxiomaticSet =
      (a,b) match {
        case (Emptyset, _) | (_, Emptyset) => Emptyset
        case (World, bm) => bm
        case (am, World) => am
        case (am, bm) if (am == bm) => am
        case (Singletons, Entities) | (Entities, Singletons) => Entities
        case (Singletons, Values) | (Values, Singletons) => Values
        case (Entities, Values) | (Values, Entities) => Emptyset
      }

    def lub(a:Option[AxiomaticSet], b:Option[AxiomaticSet]) : Option[AxiomaticSet] = {
      (a,b) match {
        case (None,_) | (_, None) => None
        case (Some(ac), Some(bc)) => Some(lub(ac,bc))
      }
    }
  }

  case class Category(id: String, name:String) extends NamedSet {
    def this(id:String) = { this(id, id) }
  }

  sealed trait NAryOpSetTerm extends SetTerm {
    val subterms: Set[SetTerm]
  }
  case class Union(subterms: Set[SetTerm]) extends NAryOpSetTerm
  case class Intersection(subterms: Set[SetTerm]) extends NAryOpSetTerm
  case class Complement(set: SetTerm, relativeTo: Option[SetTerm]) extends SetTerm
  case class CartesianProduct(sets: List[SetTerm]) extends SetTerm
  case class Relations(
                        domain: SetTerm, codomain: SetTerm,
                        areAllTotal: Boolean, areAllSurjective: Boolean,
                        areAllFunctional: Boolean, areAllInjective: Boolean
                      ) extends SetTerm

  sealed trait Property // { override def equals(obj: Any): Boolean = this == obj }
  case class Subset(subset: SetTerm, superset: SetTerm, isStrict: Boolean) extends Property
  case class In(element: SetTerm, set: SetTerm) extends Property

  trait NAryProperty extends Property {
    val terms: Set[SetTerm]
  }
  case class Equal(terms: Set[SetTerm]) extends NAryProperty
  case class Distinct(terms: Set[SetTerm]) extends NAryProperty

  sealed trait Predicate extends Property {
    val set: SetTerm
  }
  case class Finite(set: SetTerm) extends Predicate
  case class Atomic(set: SetTerm) extends Predicate
  case class Total(set: SetTerm) extends Predicate
  case class Functional(set: SetTerm) extends Predicate
  case class Surjective(set: SetTerm) extends Predicate
  case class Injective(set: SetTerm) extends Predicate

  /** ************************************************************************** */

  /**
   * Object containing the implicit functions to "equip" (or "unequip) a SetTerm
   * with useful functions.
   */
  object EquippedSetTerm {
    implicit def equip(st: SetTerm): EquippedSetTerm = EquippedSetTerm(st)
    implicit def unequip(est: EquippedSetTerm): SetTerm = est.term
  }

  case class EquippedSetTerm(term: SetTerm) {

    /**
     * Returns all the NamedSets contained in this (Equipped)SetTerm.
     * @return the NamedSets contained.
     */
    def getNamedSets: Set[NamedSet] = {
      term match {
        case ns : NamedSet => Set(ns)
        case t : NAryOpSetTerm => t.subterms.flatMap(st => st.getNamedSets)
        case CartesianProduct(sets) => sets.toSet[SetTerm].flatMap(st => st.getNamedSets)
        case Complement(set, relativeTo) =>
          set.getNamedSets ++ relativeTo.toSet[SetTerm].flatMap(st => st.getNamedSets)
        case Relations(dom, codom, _, _, _, _) => dom.getNamedSets ++ codom.getNamedSets
      }
    }

    /**
     * Returns the width (or arity) of this (equipped) SetTerm, if it can compute
     * it with the information contained in {@link env}. Otherwise, it returns None.
     * @param env known {@see Category} width (arity).
     * @return this term width (or arity) if it can be computed.
     */
    def getWidth(env:Map[Category,Int]) : Option[Int] = { // or arity
      term match {
        case World | Emptyset => None
        case Singletons | Entities | Values => Some(1)
        case c: Category => env.get(c)
        case CartesianProduct(subterms) => Some(subterms.size)
        case Complement(_, superset) => superset.getOrElse(World).getWidth(env)
        case _ : Relations => Some(2)
      }
    }

    /**
     * Returns a function which computes the axiomatic set term (a set term whose
     * leaves are axiomatic sets) which is the "bound" (lub or glb) of this
     * term and the other term provided as parameter, assuming they are both axiomatic
     * set terms.
     *
     * @param boundingOp a binary operation computing the "bound" of two elements
     * @param zero the absorbing element (or annihilating element) for the bounding
     *             operation
     * @param neutral the neutral element for the bounding operation
     * @return the above mentioned function
     */
    private def axiomaticBound(
      boundingOp : (AxiomaticSet, AxiomaticSet) => AxiomaticSet,
      zero : AxiomaticSet,
      neutral : AxiomaticSet
    ) : SetTerm => SetTerm = {
      def thisAxiomaticBoundFct(t:EquippedSetTerm): SetTerm => SetTerm =
        t.axiomaticBound(boundingOp, zero, neutral)
      other => {
        (this: SetTerm, other) match {
          case (n: AxiomaticSet, other) if n == neutral => other
          case (other, n: AxiomaticSet) if n == neutral => other
          case (a: AxiomaticSet, b: AxiomaticSet) => boundingOp(a, b)
          case (CartesianProduct(aSts), CartesianProduct(bSts))
            if aSts.length == bSts.length =>
            CartesianProduct(
              (aSts.zip(bSts): List[(SetTerm, SetTerm)]).map[SetTerm](
                { case (aSt, bSt) => thisAxiomaticBoundFct(aSt: EquippedSetTerm)(bSt) }
              )
            )
          case
            (
              Relations(domA, codA, _, _, _, _),
              Relations(domB, codB, _, _, _, _)
              )
          =>
            Relations(
              thisAxiomaticBoundFct(domA: EquippedSetTerm)(domB),
              thisAxiomaticBoundFct(codA: EquippedSetTerm)(codB),
              areAllTotal = false, areAllSurjective = false,
              areAllFunctional = false, areAllInjective = false
            )
          case _ => zero
        }
      }
    }

    /**
     * The function which computes the axiomatic set term (a set term whose
     * leaves are axiomatic sets) which is the smallest superset (or Least Upper
     * Bound, a.k.a lub) of this term and the other term provided as parameter,
     * assuming they are both axiomatic set terms.
     */
    val axiomaticLUB : SetTerm => SetTerm =
      axiomaticBound(AxiomaticSets.lub, World, Emptyset)

    /**
     * The function which computes the axiomatic set term (a set term whose
     * leaves are axiomatic sets) which is the biggest subset (or Greatest Lower
     * Bound, a.k.a glb) of this term and the other term provided as parameter,
     * assuming they are both axiomatic set terms.
     */
    val axiomaticGLB : SetTerm => SetTerm =
      axiomaticBound(AxiomaticSets.glb, Emptyset, World)

    /**
     * The operator which computes the axiomatic set term (a set term whose leaves
     * are axiomatic sets) which is the smallest superset (or Least Upper Bound,
     * a.k.a glb) of the two terms provided as parameter, assuming they are both
     * axiomatic set terms.
     *
     * @see axiomaticLUB
     */
    private val optLUB: (Option[SetTerm], Option[SetTerm]) => Option[SetTerm] =
      (aOpt,bOpt) => (aOpt, bOpt) match {
        case (None, _) | (_, None) => None
        case (Some(a), Some(b)) => Some(a.axiomaticLUB(b))
      }

    /**
     * The operator which computes the axiomatic set term (a set term whose leaves
     * are axiomatic sets) which is the biggest subset (or Greatest Lower Bound,
     * a.k.a glb) of the two terms provided as parameter, assuming they are both
     * axiomatic set terms.
     *
     * @see axiomaticGLB
     */
    private val optGLB: (Option[SetTerm], Option[SetTerm]) => Option[SetTerm] =
      (aOpt,bOpt) => (aOpt, bOpt) match {
        case (None, _) | (_, None) => None
        case (Some(a), Some(b)) => Some(a.axiomaticGLB(b))
      }

    /**
     * Computes the axiomatic set term (a set term whose leaves are axiomatic sets)
     * which is the smallest superset (or Least Upper Bound, a.k.a lub) of this
     * set ter, if it is possible to do compute it using the provided environment.
     *
     * @param env a "typing" environment containing the known axiomatic supersets
     *            of some category set terms
     * @return the smallest axiomatic superset of this set term
     */
    def getAxiomaticSuperset(env:Map[Category,SetTerm]) : Option[SetTerm] = {
      term match {
        case t : AxiomaticSet => Some(t)
        case c : Category => env.get(c)
        case Union(subterms) =>
            subterms.foldLeft(Some(Emptyset):Option[SetTerm])(
              (acc, t) => optLUB(acc, t.getAxiomaticSuperset(env))
            )
        case Intersection(subterms) =>
            subterms.foldLeft(Some(World):Option[SetTerm])(
              (acc, t) => optGLB(acc, t.getAxiomaticSuperset(env)) // !!!!!
            )
        case Complement(_, None) => Some(World)
        case Complement(subset, Some(superset)) if subset == superset => Some(Emptyset)
        case Complement(_, Some(superset)) => superset.getAxiomaticSuperset(env)
        case CartesianProduct(Nil) => Some(CartesianProduct(Nil))
        case CartesianProduct(hd :: tl) =>
          (hd.getAxiomaticSuperset(env), CartesianProduct(tl).getAxiomaticSuperset(env)) match {
            case (None, _) | (_, None) => None
            case (Some(hdAS), Some(CartesianProduct(tlAS))) => Some(CartesianProduct(hdAS :: tlAS))
          }
        case Relations(dom, codom, tP, sP, fP, iP) =>
          (dom.getAxiomaticSuperset(env), codom.getAxiomaticSuperset(env)) match {
            case (None, _) | (_, None) => None
            case (Some(domAS), Some(codomAS)) => Some(Relations(domAS, codomAS, tP, sP, fP, iP))
          }
      }
    }

  }

  /** ************************************************************************** */

  object EquippedProperty {
    implicit def equip(st: Property): EquippedProperty = EquippedProperty(st)
    implicit def unequip(est: EquippedProperty): Property = est.prop
  }

  case class EquippedProperty(prop: Property) {

    /**
     * Returns all the NamedSets contained in this (Equipped)Property.
     * @return the NamedSets contained.
     */
    def getNamedSets: Set[NamedSet] = {
      prop match {
        case p: Predicate => p.set.getNamedSets
        case p: NAryProperty => p.terms.flatMap(t => t.getNamedSets)
        case Subset(subset, superset, _) => subset.getNamedSets ++ superset.getNamedSets
        case In(_, set) => set.getNamedSets
      }
    }
  }

}
