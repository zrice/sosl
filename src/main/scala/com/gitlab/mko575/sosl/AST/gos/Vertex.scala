/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.AST.gos

import com.gitlab.mko575.sosl.AST.commons.NamedSet

sealed trait Vertex

sealed trait NamedVertex extends Vertex {
  val name: String
}

sealed trait SetVertex extends NamedVertex {
  override val name: String = set.name
  val set: NamedSet
}
case class UntypedSet(set: NamedSet) extends SetVertex
case class EntitySet(set: NamedSet) extends SetVertex
case class ValueSet(set: NamedSet) extends SetVertex

sealed trait InstanceVertex extends NamedVertex
