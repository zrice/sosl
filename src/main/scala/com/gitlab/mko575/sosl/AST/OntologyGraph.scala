/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.AST

import com.gitlab.mko575.sosl.AST.commons.{NamedSet, SetId}

import scala.collection.mutable.{Map => MMap, Set => MSet}

package gos {

  /**
   * Class representing ontologies in a graph format
   */
  case class OntologyGraph(
                       vertices: Map[SetId, Vertex],
                       edges: Set[Edge]
                     ) {

    def this(vertices: MMap[SetId, Vertex], edges: MSet[Edge]) = {
      this(vertices.toMap, edges.toSet)
    }

    /**
     * Merges two ontology graph in a new one.
     *
     * @param other the other graph to merge into this graph
     * @return a new graph resulting from merging the two graphs
     */
    def mergeWith(other: OntologyGraph) : OntologyGraph = {
      OntologyGraph(
        vertices ++ other.vertices,
        edges ++ other.edges
      )
    }

    /**
     * Returns the set of vertices representing entities
     *
     * @return the set of vertices representing entities
     */
    private def extractEntities() : Set[EntitySet] =
      vertices.values.collect({case c : EntitySet => c}).toSet
        //.asInstanceOf[Set[EntityClass]]

    /**
     * Returns the "attributes" (edges from an entity to a value) of a given
     * entity ({@see EnityClass}).
     *
     * @param entity the entity for which to retrieve "attributes"
     * @return the "attributes" of the provided entity
     */
    private def extractEntityAttributes(entity : EntitySet) : Set[HasAttribute] =
      edges.collect(
        {case edge @ HasAttribute(e, _, _) if e == entity => edge}
      )

    /**
     * Produces a string representing this graph as a UML class diagram in the
     * dot format of GraphViz.
     *
     * @return the dot string representing this grpah
     */
    def toClassDiagramWithDot : String = {
      val entities : Set[EntitySet] = extractEntities()
      val inheritances : Set[IsSubsetOf] =
        edges.collect({case edge : IsSubsetOf => edge})
      val associations : Set[Association] =
        edges.collect({case edge : Association => edge})
      s"""
      |digraph G  {
      |  rankdir=BT;
      |  node [margin=0.04];
      |
      |${
        (for (entity <- entities) yield {
          // val id : SetId = entity.name // entity.set.id
          val name : String = entity.set.name
          val attributes : Set[HasAttribute] = extractEntityAttributes(entity)
          val attrStr : String = attributes.foldLeft("")(
            (s,a) => s"""$s${a.attrName} : ${a.attrType.vertex.name}\\l"""
          )
          s"""  ${entity.name} [ shape=\"Mrecord\", style=filled, fillcolor=lightyellow,
             | label=\"{$name|$attrStr}\" ]""".stripMargin
        }
        ).mkString("\n")
      }
      |
      |  edge [arrowhead="empty"] // Inheritance relations [arrowhead=onormal]
      |${
        (for (inherit <- inheritances) yield {
          val subset : NamedVertex = inherit.from // .set.id
          val superset : NamedVertex = inherit.to // .set.id
          s"""  ${subset.name} -> ${superset.name}""".stripMargin
        }
        ).mkString("\n")
      }
      |
      |  edge [arrowhead=none, arrowtail=diamond] // Composit associations
      |
      |  edge [dir=both, arrowsize=0.5, arrowtail=nonenonenoneinv, arrowhead=none] // Reference associations : none
      |${
        (for (assoc <- associations) yield {
          val from : NamedVertex = assoc.from.vertex // set.id
          val to : NamedVertex = assoc.to.vertex // .set.id
          val name : String = assoc.name
          val fromArity : String = assoc.from.arity.getOrElse(Arity.any).toUMLString
          val toArity : String = assoc.to.arity.getOrElse(Arity.any).toUMLString
          s"""  ${from.name} -> ${to.name} [ fontsize=8, label=\"$name\", taillabel=\"$fromArity\", headlabel=\"$toArity\" ]
             |""".stripMargin
        }
        ).mkString("\n")
      }
      |}
      |""".stripMargin
    }

  }

}
