/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.AST.gos

sealed trait Edge {
  val name: String
  val connections: Set[EdgeEndpoint]
}

class BinaryEdge(
                  override val name: String,
                  tail: EdgeEndpoint,
                  head: EdgeEndpoint
                ) extends Edge {
  override val connections = Set(tail, head)
}

case class IsSubsetOf(from: SetVertex, to: SetVertex)
  extends BinaryEdge(
    "subsetOf",
    EdgeEndpoint(None, None, from),
    EdgeEndpoint(None, None, to))

case class IsInstanceOf(from: InstanceVertex, to: SetVertex)
  extends BinaryEdge("subsetOf", EdgeEndpoint(None, None, from), EdgeEndpoint(None, None, to))

case class HasAttribute(entity: EntitySet, attrName: String, attrType: EdgeEndpoint)
  extends BinaryEdge(attrName, EdgeEndpoint(None, None, entity), attrType) {

  def this(entity: EntitySet, attrName: String, attrType: ValueSet) =
    this(entity, attrName, EdgeEndpoint(Some(attrName), None, attrType))
}

case class Association(override val name: String, from: EdgeEndpoint, to: EdgeEndpoint)
  extends BinaryEdge(name, from, to) {

  def this(name: String, from: EntitySet, to: EntitySet) =
    this(name: String, EdgeEndpoint(None, None, from), EdgeEndpoint(None, None, to))
}
