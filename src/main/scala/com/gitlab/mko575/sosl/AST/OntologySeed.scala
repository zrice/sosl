/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.AST

package sos {

  import com.gitlab.mko575.sosl.AST.commons.EquippedSetTerm.equip
  import com.gitlab.mko575.sosl.AST.commons.{AxiomaticSet, AxiomaticSets, CartesianProduct,
    Category, Emptyset, Entities, EquippedProperty, EquippedSetTerm, In, NamedSet,
    Property, Relations, SetId, SetTerm, Subset, Values, World}
  import com.gitlab.mko575.sosl.AST.gos.{Arity, Association, Edge, EdgeEndpoint,
    EntitySet, HasAttribute, IsSubsetOf, OntologyGraph, SetVertex, ValueSet}
  import com.mxgraph.view.mxGraph

  import scala.collection.mutable.{Map => MMap, Set => MSet}
  import scala.collection.immutable.{Map => IMap, Set => ISet}


  /**
   * Class representing ontology seeds.
   *
   * @param id         Unique identifier (in its namespace) of the ontology seed.
   * @param name       Name of the ontology seed.
   * @param sets       Set of all categories in the seed, indexed by their identifier.
   * @param properties Properties of the categories of the ontology seed.
   */
  case class OntologySeed(
                      /** x */
                      id: String,

                      name: String,

                      /** y */
                      sets: Map[SetId, Category],

                      /** z */
                      properties: Set[Property]
                    ) {

    /**
     * Constructor handling the case where the map of categories is provided as a mutable map.
     *
     * @param id         the id and name of the seed.
     * @param sets       the categories
     * @param properties the properties
     */
    def this(
              id: String,
              sets: MMap[SetId, Category],
              properties: MSet[Property]
            ) = {
      this(id, id, sets.toMap, properties.toSet)
    }

    /**
     * Provides a verbose description of the ontology.
     *
     * @return A String describing the ontology seed.
     */
    def toMultilineVerboseString: String = {
      val categories = sets.values.map(c => c.id).addString(new StringBuilder, ", ")
      val properties =
        this.properties.foldLeft("")((acc: String, p: Property) => s"$acc\n   - ${p.toString}")
      s"""Ontology '$name' ($id) :
         | - list of categories: $categories
         | - list of properties: $properties""".stripMargin
    }

    /**
     * Extract the axiomatic superset categories from a wider environment.
     * @param env the original environment
     * @return an environment mapping category to their axiomatic superset
     */
    private def extractCategoriesAS(env:Map[SetTerm,SetTerm]) : Map[Category,SetTerm] =
      env.foldLeft(Map[Category,SetTerm]())({
        case (acc, (k:Category,v)) => acc + (k -> v)
        case (acc, _) => acc
      })

    /**
     * For every top level set term T appearing in the provided list of properties,
     * tries to compute the axiomatic set term (set term whose leaves are axiomatic
     * sets) which is T's smallest superset (or Least Upper Bound, a.k.a lub),
     * if it is possible to compute it with the provided environment containing
     * the known axiomatic supersets of some set terms.
     *
     * @param env a "typing" environment containing the known axiomatic supersets
     *            of some set terms
     * @param toProcess the properties to process
     * @param delayedProcessing the properties whose processing has been delayed
     *                          due to missing information
     * @return the known smallest axiomatic supersets after processing
     */
    private def computeAxiomaticSupersets(
      env : Map[SetTerm, SetTerm] // the values are supposed to be smallest axiomatic supersets.
    )(
      toProcess : List[Property],
      delayedProcessing : List[Property]
    ) : Map[SetTerm, SetTerm] = {
      toProcess match {
        case Nil => env
        case (head @ Subset(subset, superset, _)) :: tail =>
          (superset:EquippedSetTerm).getAxiomaticSuperset(extractCategoriesAS(env)) match {
            case None =>
              computeAxiomaticSupersets(env)(tail, head :: delayedProcessing)
            case Some(axiomaticSuperset) =>
              val updatedAS : SetTerm =
                env.getOrElse[SetTerm](subset, World).axiomaticGLB(axiomaticSuperset)
              computeAxiomaticSupersets(
                env + (subset ->  updatedAS)
              )(
                delayedProcessing.reverse ::: tail,
                Nil
              )
          }
        case _ :: tail => computeAxiomaticSupersets(env)(tail, delayedProcessing)
      }
    }

    /**
     * Extracts the categories corresponding to entities or values.
     *
     * @return the pair composed of, first, the "entities" categories and then
     *         the values categories
     */
    def extractEntitiesAndValues : (Set[Category], Set[Category]) = {
      val subsetProps : List[Property] =
        properties.collect({case p:Subset => p})
          .toList.sortBy[Int]((p:Property) => (p:EquippedProperty).getNamedSets.size)
        //properties.collect({case p@Subset(_,t,_) => p})
          // (t:EquippedSetTerm).getWidth(Map[Category,Int]()) == Some(1)

      val axiomaticSetsTyping : Map[SetTerm,SetTerm] =
        AxiomaticSets.content.foldLeft(Map[SetTerm,SetTerm]())((m,s) => m + (s -> s))
      val types = computeAxiomaticSupersets(axiomaticSetsTyping)(subsetProps, Nil)

      types.removedAll(axiomaticSetsTyping.keys)
        .foldLeft((Set[Category](), Set[Category]()))({
          case ((eSet, vSet), (st:Category, axiomaticLUB)) => {
            axiomaticLUB match {
              case Entities => (eSet + st, vSet)
              case Values => (eSet, vSet + st)
              case _ => (eSet, vSet)
            }
          }
          case (acc, _) => acc
        })
    }

    /**
     * Converts this set-based ontology ({@link OntologySeed}) to a graph-based
     * ontology ({@link OntologyGraph}) containing only the class inheritance graph.
     * @return a graph-based ontology representing class inheritances in this set-based ontology.
     */
    def toInheritanceGraph: OntologyGraph = {
      val (entities, values) : (Set[Category], Set[Category]) = extractEntitiesAndValues
      val vertices : Map[SetId, SetVertex] = (
        entities.foldLeft(Map[SetId, SetVertex]())(
          (map, eCat) => map + (eCat.id -> EntitySet(eCat))
        )
        ++
        values.foldLeft(Map[SetId, SetVertex]())(
          (map, vCat) => map + (vCat.id -> ValueSet(vCat))
        )
      )

      val edges = properties.foldLeft(MSet[Edge]())({
        case (edges, Subset(subset : Category, superset : Category, _))
          if Set[SetId](subset.id, superset.id) subsetOf vertices.keySet
        =>
          edges | Set(IsSubsetOf(vertices.get(subset.id).head, vertices.get(superset.id).head))
        case (edges, In(r : NamedSet, Relations(f : Category, t : Category, _, _, _, _)))
        if (entities contains f) && (values contains t)
        =>
          edges | Set(new HasAttribute(EntitySet(f), r.name, ValueSet(t)))
        case (edges, _) => edges
      })

      OntologyGraph(vertices, edges.toSet)
    }

    def toOntologyGraph: OntologyGraph = {
      val inheritanceOntology = toInheritanceGraph
      val (entities, values) : (Set[Category], Set[Category]) = extractEntitiesAndValues
      val vertices = inheritanceOntology.vertices

      val edges = properties.foldLeft(MSet[Edge]())({
        case (edges, In(rel : Category, Relations(dom : Category, codom : Category, total, surj, func, inj)))
          if (entities contains dom) && (entities contains codom)
        =>
          val domArity =
            (inj, surj) match {
              case (true, true) => Arity.one
              case (true, false) => Arity.optional
              case (false, true) => Arity.some
              case (false, false) => Arity.any
            }
          val codomArity =
            (total, func) match {
              case (true, true) => Arity.one
              case (true, false) => Arity.some
              case (false, true) => Arity.optional
              case (false, false) => Arity.any
            }
          val domEP = EdgeEndpoint(None, Some(domArity), EntitySet(dom))
          val codomEP = EdgeEndpoint(None, Some(codomArity), EntitySet(codom))
          edges | Set(Association(rel.name, domEP, codomEP))
        case (edges, Subset(rel : Category, CartesianProduct(_ @(dom:Category) :: (codom:Category) :: Nil), _))
          if (entities contains dom) && (entities contains codom)
        =>
          val domEP = EdgeEndpoint(None, Some(Arity.any), EntitySet(dom))
          val codomEP = EdgeEndpoint(None, Some(Arity.any), EntitySet(codom))
          edges | Set(Association(rel.name, domEP, codomEP))
        case (edges, _) => edges
      })

      OntologyGraph(vertices, inheritanceOntology.edges ++ edges)
    }

    def toMXGraph: mxGraph = {
      val graph = new mxGraph
      val parent = graph.getDefaultParent
      val vertexMap = scala.collection.mutable.Map[Object, Object]()

      def createVertex(s: NamedSet): Unit = {
        val v = graph.insertVertex(parent, s.id.toString, s.name, 0.0, 0.0, 10.0, 10.0)
        vertexMap += (s.id -> v)
      }

      graph.getModel.beginUpdate()
      for ((_, c) <- this.sets) createVertex(c)
      for (prop <- this.properties) {
        prop match {
          case Subset(sub: NamedSet, sup: NamedSet, _) =>
            if (!vertexMap.contains(sub.id)) createVertex(sub)
            if (!vertexMap.contains(sup.id)) createVertex(sup)
            graph.insertEdge(parent, prop.toString, prop, vertexMap(sub.id), vertexMap(sup.id))
          case _ => ()
        }
      }
      graph.getModel.endUpdate()
      graph
    }
  }

}
