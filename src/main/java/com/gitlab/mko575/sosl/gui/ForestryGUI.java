/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.gui;

import com.gitlab.mko575.sosl.Forestry;
import com.mxgraph.view.mxGraph;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Main class handling Forestry's GUI.
 *
 * @see Application
 * @see Platform
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
public class ForestryGUI {

  /**
   * The object at the root of Forestry's GUI. It is initialized (not {@code null})
   * if only if the main JavaFX Application Thread (see {@link Platform}) has
   * been initialized.
   */
  private static ForestryGUI gui;

  /**
   * The main stage/window of Forestry's GUI.
   */
  private Stage mainStage; // NOPMD : SonarLint is smarter

  /**
   * Create and returns the main {@link Stage} for the GUI.
   * This method has to be called inside {@link Platform#runLater(Runnable)}.
   *
   * @return the main GUI stage/window
   */
  private Stage createMainStage() {
    final String javaVersion = System.getProperty("java.version");
    final String javafxVersion = System.getProperty("javafx.version");
    final Label label = new Label(
        "Hello, JavaFX " + javafxVersion
        + ", running on Java " + javaVersion + ".");
    label.setPadding(new Insets(5));
    final StackPane pane = new StackPane(label);
    pane.autosize();
    final Scene scene = new Scene(pane);

    mainStage = new Stage();
    mainStage.setScene(scene);
    mainStage.sizeToScene();
    mainStage.setTitle("Forestry's Main Stage");
    mainStage.setOnCloseRequest(e -> Platform.runLater(Platform::exit));
    return mainStage;
  }

  /**
   * Initialize the main JavaFX Application Thread (using {@link Platform#startup(Runnable)})
   * and the main GUI object {@link #gui}, if it has not already been done.
   *
   * @see Application
   */
  private static void initGUI() {
    if (gui == null) {
      final CountDownLatch startupLatch = new CountDownLatch(1);
      Platform.startup(startupLatch::countDown);
      // Wait for the JavaFX Application Thread to be started
      try {
        startupLatch.await();
      } catch (InterruptedException e) {
        Forestry.mainAppLogger.log(// NOPMD
            Level.WARNING,
            "Interrupted while waiting for the JavaFX Application Thread to be started."
                + " I will try to continue anyway, but further exceptions may be triggered."
        );
      }
      gui = new ForestryGUI();
      // To create a main stage/window ...
      Platform.runLater(() -> {
        gui.createMainStage().show();
      });
    }
  }

  /**
   * Accessor for the main GUI object.
   *
   * @return the main GUI object
   */
  public static ForestryGUI get() {
    initGUI();
    return gui;
  }

  /**
   * Display a new window depicting the ontology graph provided as parameter.
   *
   * @param graph the ontology graph to display
   */
  public static void viewOntologyGraph(final mxGraph graph) {
    initGUI();
    Platform.runLater(() -> {
      final OntologyGraphViewer ogv = new OntologyGraphViewer(graph);
      ogv.show();
    });
  }
}
