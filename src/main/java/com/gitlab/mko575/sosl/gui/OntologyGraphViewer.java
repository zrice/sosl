/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.gui;

import com.gitlab.mko575.sosl.gui.jgraphx.JGXOntologyGraphComponent;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.view.mxGraph;
import java.awt.Dimension;
import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.annotation.Nonnull;
import javax.swing.JComponent;

/**
 * Class used to create a window ({@link Stage}) displaying an ontology seed.
 */
class OntologyGraphViewer extends Stage {

  /**
   * The component containing the graph.
   */
  @Nonnull
  private final OntologyGraphComponent ontologyGraphComponent; // NOPMD

  /**
   * Constructor of a seed viewer using JGraphX.
   * @param graph the graph to display using JGraphX technology.
   * @see <a href="https://github.com/jgraph/jgraphx">JGraphX website</a>
   */
  public OntologyGraphViewer(final mxGraph graph) {
    super();
    graph.getModel().beginUpdate();
    graph.setAutoSizeCells(true);
    for (final Object v : graph.getChildVertices(graph.getDefaultParent())) {
      graph.updateCellSize(v);
    }
    new mxHierarchicalLayout(graph).execute(graph.getDefaultParent());
    graph.getModel().endUpdate();
    ontologyGraphComponent = new JGXOntologyGraphComponent(graph);
    int width = (int) graph.getGraphBounds().getWidth();
    int height = (int) graph.getGraphBounds().getWidth();
    ontologyGraphComponent.getJComponent().setSize(new Dimension(width, height));
    setupStage();
  }

  /**
   * Setup the {@link Stage} once {@link #ontologyGraphComponent} has been set.
   */
  private void setupStage() {
    assert ontologyGraphComponent != null;

    final Label label = new Label("This si a label");

    final JComponent graph = ontologyGraphComponent.getJComponent();
    graph.setPreferredSize(new Dimension(400, 400));
    final SwingNode swingNode = new SwingNode();
    swingNode.setContent(graph);
    swingNode.autosize();

    final VBox pane = new VBox(5);
    pane.getChildren().add(label);
    pane.getChildren().add(swingNode);
    pane.autosize();

    setTitle("Ontology Seed Viewer");
    // stage.setScene(new Scene(pane, 250, 150));
    setScene(new Scene(pane));
    sizeToScene();
  }

}
