package com.gitlab.mko575.sosl.parser;

import com.gitlab.mko575.sosl.AST.commons.Category;
import com.gitlab.mko575.sosl.AST.commons.Property;
import com.gitlab.mko575.sosl.parser.SOSLParser.AtomicFormulaContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.OntologyDefBodyContext;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.misc.Pair;

/**
 * Visitor class transforming ontology seed definition body, as parsed by {@link SOSLParser},
 * into the core of Abstract Syntax Trees, as defined by {@link Category} and {@link Property}.
 */
public class CST2ASTVisitor4OntologyDefBody
    extends SOSLOverridingAssertedVisitor<Pair<Map<Object, Category>, Set<Property>>> {

  /**
   * Visitor used by visitors of this class to visit {@link AtomicFormulaContext}.
   */
  private final CST2ASTVisitor4AtomicFormulae afVisitor = // NOPMD
      new CST2ASTVisitor4AtomicFormulae();

  @Override
  public Pair<Map<Object, Category>, Set<Property>> visitOntologyDefBody(
      final OntologyDefBodyContext ctx
  ) {
    final Set<String> categoryIds = new HashSet<>(); // NOPMD
    final Set<Property> properties = new HashSet<>(); // NOPMD
    for (AtomicFormulaContext afCtx : ctx.atomicFormula()) { // NOPMD: afCtx is not 'final'!
      final Pair<Set<String>, Collection<Property>> afRes = afVisitor.visit(afCtx);
      assert afRes != null
          : String.format(
              "The result returned by CST2ASTVisitor4AtomicFormulae for '%s' should not be null.",
              afCtx.getText());
      assert afRes.a != null
          : String.format(
              "The set of categories returned by CST2ASTVisitor4AtomicFormulae"
                  + " for '%s' should not be null.",
              afCtx.getText());
      categoryIds.addAll(afRes.a);
      assert afRes.b != null
          : String.format(
              "The collection of properties returned by CST2ASTVisitor4AtomicFormulae"
                  + " for '%s' should not be null.",
              afCtx.getText());
      properties.addAll(afRes.b);
    }
    return new Pair<Map<Object, Category>, Set<Property>>(
        (Map<Object, Category>)
        categoryIds.stream().collect(
            Collectors.toMap(id -> (Object) id, Category::new) // Function.identity()
        ),
        properties);
  }
}
