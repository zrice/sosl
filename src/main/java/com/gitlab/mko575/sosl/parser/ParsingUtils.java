package com.gitlab.mko575.sosl.parser;

import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.misc.ParseCancellationException;

/**
 * Utility class used to initialize and retrieve lexers and parsers for SOSL files.
 */
public final class ParsingUtils {

  /**
   * Private constructor throwing an exception to discourage instantiating or inheriting from this
   * class.
   *
   * @throws AssertionError always
   */
  private ParsingUtils() throws AssertionError {
    throw new AssertionError(
        "ParsingUtils is a utility class."
            + " No instance of it should ever be created!");
  }

  /**
   * Creates a {@link CharStream} for parsing from a {@link String}.
   *
   * @param srcTxt String to parse
   * @return {@link CharStream} corresponding to the {@link String} to parse.
   */
  private static CharStream getCharStreamFromString(final String srcTxt) {
    return CharStreams.fromString(srcTxt);
  }

  /**
   * Creates a {@link CharStream} for parsing from a file path.
   *
   * @param srcFileName Path of the file to parse.
   * @return {@link CharStream} corresponding to the file to parse.
   * @throws IOException for the same reasons as {@link CharStreams#fromFileName}.
   */
  private static CharStream getCharStreamFromFileName(
      final String srcFileName
  ) throws IOException {
    return CharStreams.fromFileName(srcFileName);
  }

  /**
   * Returns a parser for its input, starting lexing in the specified mode.
   *
   * @param stream input to parse
   * @param startingMode mode i, which to start lexing.
   * @return a parser for the provided input
   */
  private static SOSLParser getParserOfCharStream(
      final CharStream stream, final int startingMode
  ) {
    final SOSLLexer lexer = new SOSLLexer(stream);
    lexer.mode(startingMode);
    return getParserForLexer(lexer);
  }

  /**
   * Returns a parser for its input.
   *
   * @param stream input to parse
   * @return a parser for the provided input
   */
  private static SOSLParser getParserOfCharStream(final CharStream stream) {
    return getParserOfCharStream(stream, Lexer.DEFAULT_MODE);
  }

  /**
   * Returns a (default test) parser for its input.
   *
   * @param lexer lexer providing tokens
   * @return a parser for the provided lexer
   */
  private static SOSLParser getParserForLexer(final SOSLLexer lexer) {
    return new SOSLParser(new CommonTokenStream(lexer));
  }

  /**
   * Returns a parser for its input, starting lexing in the specified mode.
   *
   * @param srcTxt the String to be parsed.
   * @param startingMode mode i, which to start lexing.
   * @return a parser for the provided input.
   */
  public static SOSLParser getParserOfString(
      final String srcTxt, final int startingMode
  ) {
    final CharStream stream = getCharStreamFromString(srcTxt);
    return getParserOfCharStream(stream, startingMode);
  }

  /**
   * Returns a parser for its input.
   *
   * @param srcTxt the String to be parsed.
   * @return a parser for the provided input.
   */
  public static SOSLParser getParserOfString(final String srcTxt) {
    return getParserOfString(srcTxt, Lexer.DEFAULT_MODE);
  }

  /**
   * Returns a parser for the content of the file provided as parameter.
   *
   * @param filePath the file to be parsed.
   * @return a parser for the provided input.
   */
  public static SOSLParser getParserOfFile(final Path filePath) throws IOException {
    final CharStream stream = getCharStreamFromFileName(filePath.toString());
    return getParserOfCharStream(stream);
  }

  /**
   * Configure the provided parser to "fail-fast" mode ( {@link PredictionMode#SLL SLL(*) prediction
   * mode} and {@link BailErrorStrategy bail error strategy}). In case of syntax error, parsing with
   * this parser returns a {@link ParseCancellationException}.
   *
   * @param parser the parser to configure
   */
  public static void setParserToFailFastMode(final SOSLParser parser) {
    parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
    parser.removeErrorListeners();
    parser.setErrorHandler(new BailErrorStrategy());
  }

  /**
   * Configure the provided parser to "try hard" mode ( {@link PredictionMode#LL LL(*) prediction
   * mode} and {@link DefaultErrorStrategy default error strategy}).
   *
   * @param parser the parser to configure
   */
  public static void setParserToTryHardMode(final SOSLParser parser) {
    parser.getInterpreter().setPredictionMode(PredictionMode.LL);
    parser.removeErrorListeners();
    parser.addErrorListener(new BaseErrorListener());
    parser.setErrorHandler(new DefaultErrorStrategy());
  }

  /**
   * Retrieve all files in {@code dirPath} whose names satisfy {@code filePattern}.
   *
   * @param dirPath path to scan.
   * @param filePattern file path pattern to look for ({@link PathMatcher#matches}).
   * @param recursively proceed recursively with subdirectories if {@code true}
   * @return the collection of matching file paths
   * @throws IOException for the same reason as {@link Files#walk(Path, int, FileVisitOption...)}
   */
  public static Stream<Path> retrieveFiles(
      final Path dirPath, final String filePattern, final boolean recursively
  ) throws IOException  {
    final PathMatcher matcher = FileSystems.getDefault().getPathMatcher(filePattern);
    final int maxDepth = recursively ? Integer.MAX_VALUE : 0;
    return Files.walk(dirPath, maxDepth).filter(matcher::matches);
  }

  /**
   * Retrieve all files in resources starting from {@code path} whose names satisfy {@code
   * filePattern}.
   *
   * @param path path to start from.
   * @param filePattern file pattern pattern to look for ({@link PathMatcher#matches}).
   * @return the collection of matching file paths
   * @throws IOException for the same reason as {@link ParsingUtils#retrieveFiles(Path, String,
   *     boolean)}
   */
  public static Stream<Path> retrieveFilesInSystemResources(
      final String path, final String filePattern
  ) throws IOException {
    final URL resourceURL = ClassLoader.getSystemResource(path);
    final Path resourcePath = Paths.get(resourceURL.getPath());
    return retrieveFiles(resourcePath, filePattern, true);
  }
}
