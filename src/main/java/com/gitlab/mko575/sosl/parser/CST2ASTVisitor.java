package com.gitlab.mko575.sosl.parser;

import com.gitlab.mko575.sosl.AST.commons.Category;
import com.gitlab.mko575.sosl.AST.commons.Property;
import com.gitlab.mko575.sosl.AST.sos.OntologySeed;
import com.gitlab.mko575.sosl.parser.SOSLParser.AtomicFormulaContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.InstanceDefinitionContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.OntologyDefinitionContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.SosFileContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.antlr.v4.runtime.misc.Pair;
import scala.jdk.javaapi.CollectionConverters;

/**
 * Top level visitor class transforming SOSL file Concrete Syntax Trees (CST),
 * as parsed by {@link SOSLParser}, into Abstract Syntax Trees, as defined by
 * {@link OntologySeed}.
 */
public class CST2ASTVisitor
    extends SOSLOverridingAssertedVisitor<Map<String, OntologySeed>> {

  /** Error message for missing cases. */
  static final String ERR_MSG_CASE_MISSING =
      "This should never have been thrown!" + " There is a case missing in method %s of class %s.";

  /**
   * Visitor used by visitors of this class to visit {@link AtomicFormulaContext}.
   */
  private final CST2ASTVisitor4OntologyDefBody odbVisitor = // NOPMD
      new CST2ASTVisitor4OntologyDefBody();

  /**
   * Current set of {@link OntologySeed}s retrieved so far.
   */
  private final Map<String, OntologySeed> sosMap = new HashMap<>(); // NOPMD

  @Override
  public Map<String, OntologySeed> visitSosFile(final SosFileContext ctx) {
    visitChildren(ctx);
    return sosMap;
  }

  @Override
  public Map<String, OntologySeed> visitOntologyDefinition(final OntologyDefinitionContext ctx) {
    final Pair<Map<Object, Category>, Set<Property>> odbRes = odbVisitor.visit(ctx.body);
    final String seedId = ctx.ontologyId.getText();
    final OntologySeed seed =
        new OntologySeed(
            seedId,
            CollectionConverters.asScala((Map<Object, Category>) odbRes.a),
            CollectionConverters.asScala((Set<Property>) odbRes.b));
    sosMap.put(seedId, seed);
    return Map.of(seedId, seed);
  }

  @Override
  public Map<String, OntologySeed> visitInstanceDefinition(final InstanceDefinitionContext ctx) {
    return Collections.emptyMap();
  }
}
