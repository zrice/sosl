package com.gitlab.mko575.sosl.parser;

import static com.gitlab.mko575.sosl.parser.CST2ASTVisitor.ERR_MSG_CASE_MISSING;

import com.gitlab.mko575.sosl.AST.commons.Atomic;
import com.gitlab.mko575.sosl.AST.commons.Distinct;
import com.gitlab.mko575.sosl.AST.commons.Equal;
import com.gitlab.mko575.sosl.AST.commons.Finite;
import com.gitlab.mko575.sosl.AST.commons.Functional;
import com.gitlab.mko575.sosl.AST.commons.In;
import com.gitlab.mko575.sosl.AST.commons.Injective;
import com.gitlab.mko575.sosl.AST.commons.Property;
import com.gitlab.mko575.sosl.AST.commons.SetTerm;
import com.gitlab.mko575.sosl.AST.commons.Subset;
import com.gitlab.mko575.sosl.AST.commons.Surjective;
import com.gitlab.mko575.sosl.AST.commons.Total;
import com.gitlab.mko575.sosl.parser.SOSLParser.BinaryPred_AFContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.EmbedablePredContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NaryPred_AFContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.UnaryPred_AFContext;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import org.antlr.v4.runtime.misc.Pair;
import scala.jdk.javaapi.CollectionConverters;

/**
 * Visitor class transforming AtomicFormulae Concrete Syntax Trees (CST), as
 * parsed by {@link SOSLParser}, into fragments of Abstract Syntax Trees, as
 * defined by {@link Property}.
 */
public class CST2ASTVisitor4AtomicFormulae
    extends SOSLOverridingAssertedVisitor<Pair<Set<String>, Collection<Property>>> {

  /**
   * Visitor used by visitors of this class to visit {@link com.gitlab.mko575.sosl.AST.Term}.
   */
  private final CST2ASTVisitor4Terms termVisitor = new CST2ASTVisitor4Terms(); // NOPMD

  @Override
  public Pair<Set<String>, Collection<Property>> visitUnaryPred_AF(final UnaryPred_AFContext ctx) {
    final Pair<Set<String>, Collection<SetTerm>> termRes = termVisitor.visit(ctx.term());
    final Set<String> idRes = termRes.a;
    final Collection<Property> propRes = new HashSet<>();

    Function<SetTerm, Property> constructor = null;
    if (ctx.pred.finite_KW() != null) {
      constructor = Finite::new;
    } else if (ctx.pred.atomic_KW() != null) {
      constructor = Atomic::new;
    } else if (ctx.pred.total_KW() != null) {
      constructor = Total::new;
    } else if (ctx.pred.functional_KW() != null) {
      constructor = Functional::new;
    } else if (ctx.pred.surjective_KW() != null) {
      constructor = Surjective::new;
    } else if (ctx.pred.injective_KW() != null) {
      constructor = Injective::new;
    } else {
      assert false : String.format(ERR_MSG_CASE_MISSING, "visitUnaryPred_AF", this.getClass());
    }
    for (SetTerm s : termRes.b) { // NOPMD: s is not 'final'!
      propRes.add(constructor.apply(s));
    }

    return new Pair<>(idRes, propRes);
  }

  /**
   * Transforms a {@link Collection} of {@link EmbedablePredContext} applied on a
   * {@link Collection} of {@link SetTerm} into a {@link Collection} of {@link Property}.
   *
   * @param setColl the {@link SetTerm}s to which the embedable predicates are applied.
   * @param embeddablePreds  the embeddable predicates to be applied.
   * @return the result of the transformation
   */
  private Collection<Property> embeddedFormulaVisitor(
      final Collection<SetTerm> setColl,
      final List<EmbedablePredContext> embeddablePreds
  ) {
    final Collection<Property> propRes = new HashSet<>();
    for (EmbedablePredContext epCtx : embeddablePreds) { // NOPMD: epCtx is not 'final'!
      if (epCtx.unaryPred() != null) {
        final Function<SetTerm, Property> newProperty;
        if (epCtx.unaryPred().atomic_KW() != null) {
          newProperty = Atomic::new;
        } else if (epCtx.unaryPred().finite_KW() != null) {
          newProperty = Finite::new;
        } else {
          assert false :
              String.format(ERR_MSG_CASE_MISSING, "embeddedFormulaVisitor", this.getClass());
          newProperty = null;
        }
        setColl.stream().forEach(s -> propRes.add(newProperty.apply(s)));
      } else if (epCtx.naryPred() != null) {
        if (epCtx.naryPred().distinct_KW() != null) {
          propRes.add(
              new Distinct(// NOPMD: I do want to create a new Object
                  CollectionConverters.asScala(new HashSet<SetTerm>(setColl)) // NOPMD
                  // CollectionConverters.asScala(setColl
                  // .stream().collect(Collectors.toSet()))
                  ));
        } else {
          assert false :
              String.format(ERR_MSG_CASE_MISSING, "embeddedFormulaVisitor", this.getClass());
        }
      }
    }
    return propRes;
  }

  @Override
  public Pair<Set<String>, Collection<Property>> visitBinaryPred_AF(
      final BinaryPred_AFContext ctx) {
    final Pair<Set<String>, Collection<SetTerm>> lhsRes = termVisitor.visit(ctx.lhs);
    final Pair<Set<String>, Collection<SetTerm>> rhsRes = termVisitor.visit(ctx.rhs);

    final Set<String> idRes = new HashSet<>();
    idRes.addAll(lhsRes.a);
    idRes.addAll(rhsRes.a);

    final Collection<Property> propRes = new HashSet<>();
    if (ctx.embeddedFormulae() != null) {
      propRes.addAll(embeddedFormulaVisitor(lhsRes.b, ctx.embeddedFormulae().embedablePred()));
    }
    BiFunction<SetTerm, SetTerm, Property> constructor = null;
    if (ctx.pred.subset_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(lhs, rhs, true);
    } else if (ctx.pred.subseteq_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(lhs, rhs, false);
    } else if (ctx.pred.superset_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(rhs, lhs, true);
    } else if (ctx.pred.superseteq_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(rhs, lhs, false);
    } else if (ctx.pred.in_KW() != null) {
      constructor = (lhs, rhs) -> new In(lhs, rhs);
    } else if (ctx.pred.equal_KW() != null) {
      constructor =
          (lhs, rhs) -> new Equal(CollectionConverters.asScala(Set.of(lhs, rhs)));
    } else {
      assert false : String.format(ERR_MSG_CASE_MISSING, "visitBinaryPred_AF", this.getClass());
    }
    for (SetTerm lhs : lhsRes.b) { // NOPMD: lhs is not 'final'!
      for (SetTerm rhs : rhsRes.b) { // NOPMD: rhs is not 'final'!
        propRes.add(constructor.apply(lhs, rhs));
      }
    }

    return new Pair<>(idRes, propRes);
  }

  @Override
  public Pair<Set<String>, Collection<Property>> visitNaryPred_AF(final NaryPred_AFContext ctx) {
    final Pair<Set<String>, Collection<SetTerm>> termRes = termVisitor.visit(ctx.term());
    final Set<String> idRes = termRes.a;
    final Collection<Property> propRes = new HashSet<>();

    if (ctx.pred.distinct_KW() != null) {
      propRes.add(new Distinct(CollectionConverters.asScala(new HashSet<SetTerm>(termRes.b))));
    } else {
      assert false : String.format(ERR_MSG_CASE_MISSING, "visitNaryPred_AF", this.getClass());
    }

    return new Pair<>(idRes, propRes);
  }
}
