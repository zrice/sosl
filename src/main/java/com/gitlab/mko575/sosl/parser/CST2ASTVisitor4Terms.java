package com.gitlab.mko575.sosl.parser; // NOPMD: High coupling between parser and AST

import static com.gitlab.mko575.sosl.parser.CST2ASTVisitor.ERR_MSG_CASE_MISSING;

import com.gitlab.mko575.sosl.AST.commons.CartesianProduct;
import com.gitlab.mko575.sosl.AST.commons.Category;
import com.gitlab.mko575.sosl.AST.commons.Complement;
import com.gitlab.mko575.sosl.AST.commons.Emptyset$;
import com.gitlab.mko575.sosl.AST.commons.Entities$;
import com.gitlab.mko575.sosl.AST.commons.Intersection;
import com.gitlab.mko575.sosl.AST.commons.Relations;
import com.gitlab.mko575.sosl.AST.commons.SetTerm;
import com.gitlab.mko575.sosl.AST.commons.Union;
import com.gitlab.mko575.sosl.AST.commons.Values$;
import com.gitlab.mko575.sosl.parser.SOSLParser.BinFunContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.BinaryFunOnSet_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.ClosingTerm_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.EmptysetIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.EntitiesIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.IdConstant_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.List_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.Set_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.UnaryFunOnSet_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.UserDefinedSetIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.ValuesIdContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.misc.Pair;
import scala.Option;
import scala.jdk.javaapi.CollectionConverters;

/**
 * Visitors of this class convert Concrete Syntax Tree (CST) terms to their corresponding collection
 * of AbstractSyntax Tree (AST) sets.
 *
 * <p>Calling one of these methods return a pair composed of: - the set of Set identifiers
 * encountered in the CST; - the set of AST corresponding to the CST.
 */
public class CST2ASTVisitor4Terms
    extends SOSLOverridingAssertedVisitor<Pair<Set<String>, Collection<SetTerm>>> {

  /* VISITING  SET CONSTANTS */

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitIdConstant_ST(final IdConstant_STContext ctx) {
    return visit(ctx.setId());
  }

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitEmptysetId(final EmptysetIdContext ctx) {
    return new Pair<>(new HashSet<>(), Collections.singleton(Emptyset$.MODULE$));
  }

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitEntitiesId(final EntitiesIdContext ctx) {
    return new Pair<>(new HashSet<>(), Collections.singleton(Entities$.MODULE$));
  }

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitValuesId(final ValuesIdContext ctx) {
    return new Pair<>(new HashSet<>(), Collections.singleton(Values$.MODULE$));
  }

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitUserDefinedSetId(
      final UserDefinedSetIdContext ctx
  ) {
    final String uid = ctx.userDefinedId().id.getText();
    return new Pair<>(Collections.singleton(uid), Collections.singleton(new Category(uid)));
  }

  /* VISITING BASIC TERM CONSTRUCTIONS */

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitClosingTerm_ST(
      final ClosingTerm_STContext ctx
  ) {
    return visit(ctx.term());
  }

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitSet_ST(final Set_STContext ctx) {
    final Set<String> idRes = new HashSet<>();
    final Set<SetTerm> setsRes = new HashSet<>();
    for (Pair<Set<String>, Collection<SetTerm>> p : // NOPMD
        ctx.term().stream().map(this::visit).collect(Collectors.toSet())
    ) {
      idRes.addAll(p.a);
      setsRes.addAll(p.b);
    }
    return new Pair<>(idRes, setsRes);
  }

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitList_ST(final List_STContext ctx) {
    final Set<String> idRes = new HashSet<>();
    final List<SetTerm> setsRes = new ArrayList<>();
    for (Pair<Set<String>, Collection<SetTerm>> p : // NOPMD
        ctx.term().stream().map(this::visit).collect(Collectors.toSet())
    ) {
      idRes.addAll(p.a);
      setsRes.addAll(p.b);
    }
    return new Pair<>(idRes, setsRes);
  }

  /* VISITING FUNCTION ON SET */

  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitUnaryFunOnSet_ST(
      final UnaryFunOnSet_STContext ctx
  ) {
    final Pair<Set<String>, Collection<SetTerm>> visitRes = visit(ctx.term());
    final Collection<SetTerm> termCST = visitRes.b;
    Collection<SetTerm> setsRes = null;
    if (ctx.fun.complement_prefix_KW() != null) {
      if (termCST instanceof List) {
        setsRes = new ArrayList<>();
      } else if (termCST instanceof Set) {
        setsRes = new HashSet<>();
      } else {
        assert false
            : String.format(ERR_MSG_CASE_MISSING, "visitUnaryFunOnSet_ST", this.getClass());
      }
      for (SetTerm s : termCST) { // NOPMD
        setsRes.add(new Complement(s, Option.empty())); // NOPMD: I do create new Object
      }
    } else {
      assert false
          : String.format(ERR_MSG_CASE_MISSING, "visitUnaryFunOnSet_ST", this.getClass());
    }
    return new Pair<>(visitRes.a, setsRes);
  }

  /**
   * Returns the correct binary function AST node constructor for the provided CST node.
   * @param fun the binary function CST node
   * @return the binary function AST node
   */
  private BinaryOperator<SetTerm> getConstructorFor(final BinFunContext fun) { // NOPMD
    final BinaryOperator<SetTerm> res;
    if (fun.union_KW() != null) {
      res =
          (lhs, rhs) -> new Union(CollectionConverters.asScala(Set.of(lhs, rhs)));
    } else if (fun.intersection_KW() != null) {
      res =
          (lhs, rhs) -> new Intersection(CollectionConverters.asScala(Set.of(lhs, rhs)));
    } else if (fun.cartesianProd_KW() != null) {
      res =
          (lhs, rhs) ->
              new CartesianProduct(
                  CollectionConverters.asScala(List.of(lhs, rhs)).toList());
    } else if (fun.partialFunction_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, false, false, true, false);
    } else if (fun.totalFunction_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, true, false, true, false);
    } else if (fun.partialInjection_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, false, false, true, true);
    } else if (fun.totalInjection_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, true, false, true, true);
    } else if (fun.partialSurjection_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, false, true, true, false);
    } else if (fun.totalSurjection_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, true, true, true, false);
    } else if (fun.partialBijection_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, false, true, true, true);
    } else if (fun.totalBijection_KW() != null) {
      res = (lhs, rhs) -> new Relations(lhs, rhs, true, true, true, true);
    } else {
      assert false :
          String.format(ERR_MSG_CASE_MISSING, "visitBinaryFunOnSet_ST", this.getClass());
      res = null;
    }
    return res;
  }
  
  @Override
  public Pair<Set<String>, Collection<SetTerm>> visitBinaryFunOnSet_ST(// NOPMD
      final BinaryFunOnSet_STContext ctx
  ) {
    final Pair<Set<String>, Collection<SetTerm>> lhsRes = visit(ctx.lhs);
    final Pair<Set<String>, Collection<SetTerm>> rhsRes = visit(ctx.rhs);

    final Set<String> idRes = new HashSet<>();
    idRes.addAll(lhsRes.a);
    idRes.addAll(rhsRes.a);

    final Collection<SetTerm> lhsCST = lhsRes.b;
    final Collection<SetTerm> rhsCST = rhsRes.b;

    Collection<SetTerm> setsRes;
    if (lhsCST instanceof List || rhsCST instanceof List) {
      setsRes = new ArrayList<>();
    } else {
      setsRes = new HashSet<>();
    }
    
    final BinaryOperator<SetTerm> constructor = getConstructorFor(ctx.fun);
    for (SetTerm lhs : lhsCST) { // NOPMD
      for (SetTerm rhs : rhsCST) { // NOPMD
        setsRes.add(constructor.apply(lhs, rhs));
      }
    }
    return new Pair<>(idRes, setsRes);
  }
}
