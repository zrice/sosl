/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.cli;

import com.gitlab.mko575.sosl.ForestryUtils;
import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;

/**
 * This class handles the CLI subcommand "cultivate" and delegates all logic to
 * {@link ForestryUtils}.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
@Command(
    name = "cultivate",
    description = "Foster the growth of a Set-based Ontology Seed (SOS).",
    subcommands = {CommandLine.HelpCommand.class},
    subcommandsRepeatable = true
)
public class CultivateCmd extends CLICmd {

  /**
   * Method called to handle the 'basic-view-jgraphx' subcommand.
   * This subcommand is used to display ontology seeds.
   *
   * @param verbosity verbosity options entered on command line
   * @param ignoreParentSOSLFiles whether or not to ignore SOSL files provided
   *                              as parameters of parent commands
   * @param soslFiles SOSL files to load
   * @param seedsDisplayed SOS to display
   */
  @Command(
      name = "basic-view-jgraphx",
      description = "Display the designated Set-based Ontology Seeds (SOS)."
  )
  void basicViewJGraphx(
      @Option(
          names = {"-v", "--verbose"},
          description = "Increase verbosity"
      )
      boolean[] verbosity,
      @Option(
          names = {"--ignore-parent-SOSL-files"},
          description = "Ignore SOSL files provided as parameters of parent commands"
      )
          boolean ignoreParentSOSLFiles,
      @Option(
          names = {"-f", "--SOSL-files"},
              arity = "1..*",
              paramLabel = "<SOSL file>",
              description = "SOSL files to load.")
          Collection<File> soslFiles,
      @Parameters(
          arity = "1..*",
          paramLabel = "<ontology>",
          description = "The ontology seeds whose content is to be displayed."
      )
      Collection<String> seedsDisplayed
  ) {
    boolean verbose = collectVerbosity(verbosity);
    soslFiles = collectSOSLFiles(ignoreParentSOSLFiles, soslFiles);
    ForestryUtils.viewUsingJGraphx(verbose, soslFiles, seedsDisplayed);
  }

}
