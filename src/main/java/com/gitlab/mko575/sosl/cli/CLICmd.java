/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.cli;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;

/**
 * Abstract class handling fields and methods common to all commands (check, cultivate
 * and harvest).
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
public abstract class CLICmd {

  @ParentCommand
  private ForestryCLI parent;

  /**
   * Verbosity options entered on the top level part of the command line.
   */
  @Option(
      names = {"-v", "--verbose"},
      description = "Increase verbosity"
  )
  boolean[] verbosity;

  /**
   * Whether or not to ignore SOSL files provided as parameters of parent commands.
   */
  @Option(
      names = {"--ignore-parent-SOSL-files"},
      description = "Ignore SOSL files provided as parameters of parent commands."
  )
  boolean ignoreParentSOSLFiles;

  /**
   * SOSL files to take into consideration for subcommands.
   */
  @Parameters(
      arity = "0..*",
      paramLabel = "<SOSL file>",
      description = "SOSL files to pass to subcommands"
  )
  Collection<File> soslFiles;

  /**
   * Returns {@code true} iff the subcommand or one of its parent has been set
   * to be verbose.
   *
   * @param verbosity the verbosity parameter of the subcommand
   * @return {@code true} iff the subcommand or one of its parent has been set
   *     to be verbose
   */
  boolean collectVerbosity(boolean[] verbosity) {
    return
        (verbosity != null && verbosity.length > 0)
            || (this.verbosity != null && this.verbosity.length > 0)
            || (parent.verbosity != null && parent.verbosity.length > 0);
  }

  /**
   * Returns all the SOSL files to take into account when processing a subcommand,
   * including those passed as parameters. The method iterate over parent's parameters
   * if needed.
   *
   * @param soslFiles the SOSL files passed directly to the subcommand
   * @return all the SOSL files to take into account
   */
  Collection<File> collectSOSLFiles(boolean ignoreParentSOSLFiles, Collection<File> soslFiles) {
    Collection<File> res = new HashSet<>();
    if (soslFiles != null) {
      res.addAll(soslFiles);
    }
    if (! ignoreParentSOSLFiles) {
      if (this.soslFiles != null) {
        res.addAll(this.soslFiles);
      }
      if (! this.ignoreParentSOSLFiles && parent.soslFiles != null) {
        res.addAll(parent.soslFiles);
      }
    }
    return res;
  }
}
