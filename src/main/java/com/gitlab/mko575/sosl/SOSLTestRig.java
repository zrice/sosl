package com.gitlab.mko575.sosl;

import com.gitlab.mko575.sosl.parser.SOSLLexer;
import com.gitlab.mko575.sosl.parser.SOSLParser;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.print.PrintException;
import org.antlr.v4.gui.TestRig;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 * Extends the {@link org.antlr.v4.gui.TestRig} class to allow to
 * programmatically call it.
 */
public class SOSLTestRig extends TestRig {

  /**
   * Calls the {@link org.antlr.v4.gui.TestRig} with arguments initializing it
   * for SOSL.
   * @throws Exception See {@link org.antlr.v4.gui.TestRig#TestRig}
   */
  public SOSLTestRig() throws Exception { //NOPMD
    super(new String[] {"SOSL", "sosFile"});
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to produce tree information.
   * @return {@code this} object.
   */
  public SOSLTestRig withTree() {
    printTree = true;
    return this;
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to show a GUI.
   * @return {@code this} object.
   */
  public SOSLTestRig withGui() {
    gui = true;
    return this;
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to output the sequence of tokens
   * returned by the lexer.
   * @return {@code this} object.
   */
  public SOSLTestRig withTokens() {
    showTokens = true;
    return this;
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to the call trace of the parser
   * execution.
   * @return {@code this} object.
   */
  public SOSLTestRig withTrace() {
    trace = true;
    return this;
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to use SLL parsing.
   * @return {@code this} object.
   */
  public SOSLTestRig withSLL() {
    SLL = true;
    return this;
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to output diagnostic information.
   * @return {@code this} object.
   */
  public SOSLTestRig withDiagnostics() {
    diagnostics = true;
    return this;
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to use the provided encoding.
   * @return {@code this} object.
   */
  public SOSLTestRig withEncoding(final String enc) {
    encoding = enc;
    return this;
  }

  /**
   * Set the {@link org.antlr.v4.gui.TestRig} to produce a Postscript file.
   * @return {@code this} object.
   */
  public SOSLTestRig withPS(final String psFile) {
    this.psFile = psFile;
    return this;
  }

  /**
   * Run the underlying {@link org.antlr.v4.gui.TestRig} (configured for SOSL
   * parsing) on the provided file.
   * @param soslFile the file to be parsed.
   * @throws InvocationTargetException See {@link org.antlr.v4.gui.TestRig#TestRig}
   * @throws PrintException See {@link org.antlr.v4.gui.TestRig#TestRig}
   * @throws IllegalAccessException See {@link org.antlr.v4.gui.TestRig#TestRig}
   * @throws IOException See {@link org.antlr.v4.gui.TestRig#TestRig}
   */
  public void process(final File soslFile)
      throws InvocationTargetException, PrintException, IllegalAccessException, IOException {
    final CharStream sosFileCS = CharStreams.fromPath(soslFile.toPath());
    final SOSLLexer lexer = new SOSLLexer(sosFileCS);
    final SOSLParser parser = new SOSLParser(new CommonTokenStream(lexer));
    parser.removeErrorListeners();
    process(lexer, parser.getClass(), parser, sosFileCS);
  }
}
