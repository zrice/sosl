/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl;

import static com.gitlab.mko575.sosl.Forestry.mainAppLogger;
import static com.gitlab.mko575.sosl.Forestry.mainAppOutput;
import static java.nio.charset.StandardCharsets.UTF_8;

import com.gitlab.mko575.sosl.AST.gos.OntologyGraph;
import com.gitlab.mko575.sosl.AST.sos.OntologySeed;
import com.gitlab.mko575.sosl.gui.ForestryGUI;
import com.gitlab.mko575.sosl.parser.CST2ASTVisitor;
import com.gitlab.mko575.sosl.parser.ParsingUtils;
import com.gitlab.mko575.sosl.parser.SOSLParser;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Collectors;
import org.antlr.v4.gui.TestRig;
import org.antlr.v4.runtime.ParserRuleContext;
import scala.jdk.javaapi.CollectionConverters;

/**
 * Static class containing the code of commands runnable from {@link Forestry}.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
public final class ForestryUtils {

  /** Error string for file paths corresponding to files that can not be read. */
  private static final String
      FILE_S_CAN_NOT_BE_FOUND_OR_READ = "File '%s' can not be found or read.";

  /**
   * Private constructor throwing an exception to discourage instantiating or
   * inheriting from this class.
   *
   * @throws AssertionError always
   */
  private ForestryUtils() throws AssertionError {
    throw new AssertionError(
        "ForestryUtils is a utility class."
            + " No instance of it should ever be created!");
  }

  /**
   * Returns the ontology seeds parsed from the provided file.
   *
   * @param verbose increase verbosity if {@code true}.
   * @param soslFile SOSL {@link File} containing SOS to retrieve.
   * @return the parsed ontology seeds.
   */
  private static Map<String, OntologySeed> retrieveSeeds(
      final boolean verbose, final File soslFile
  ) {
    if (!soslFile.canRead()) {
      final String errorMsg = String.format(FILE_S_CAN_NOT_BE_FOUND_OR_READ, soslFile.getPath());
      mainAppOutput.out(errorMsg);
      if (mainAppLogger.isLoggable(Level.SEVERE)) {
        mainAppLogger.severe(errorMsg);
      }
    }

    Map<String, OntologySeed> ontologySeeds = new HashMap<>(); // NOPMD
    try {
      final SOSLParser parser = ParsingUtils.getParserOfFile(soslFile.toPath());
      final ParserRuleContext cst = parser.sosFile();
      if (cst != null) {
        ontologySeeds = new CST2ASTVisitor().visit(cst);
        if (verbose) { // NOPMD
          if (ontologySeeds.isEmpty()) {
            mainAppOutput.out(String.format("Found no ontology in '%s':", soslFile.getPath()));
          } else {
            mainAppOutput.out("Found the following ontologies!");
            ontologySeeds.forEach(
                (id, sos) -> {
                  mainAppOutput.out();
                  mainAppOutput.out(sos.toMultilineVerboseString());
                });
          }
        }
      }
    } catch (IOException e) {
      mainAppLogger.log(
          Level.SEVERE,
          "Error while opening ''{0}'': {1}",
          new String[]{soslFile.getPath(), e.getMessage()}
      );
    }
    return ontologySeeds;
  }

  /**
   * Produces a class diagram (using Graphviz) of the designated ontologies.
   *
   * @param verbose increase verbosity if {@code true}
   * @param soslFiles collection of files containing the SOS to harvest
   * @param seedsAsClassDiagram the seeds to be displayed in the class diagram
   * @param outputPath path prefix of all output files
   * @param outputFormats file formats to produce
   */
  public static void produceClassDiagramWithGraphviz(
      final boolean verbose,
      final Collection<File> soslFiles,
      final Collection<String> seedsAsClassDiagram,
      final File outputPath,
      final Collection<String> outputFormats
  ) {
    final Map<String, OntologySeed> ontologySeeds = new HashMap<>();
    for (File soslFile : soslFiles) {
      ontologySeeds.putAll(retrieveSeeds(verbose, soslFile));
    }
    if (seedsAsClassDiagram != null) {
      if (seedsAsClassDiagram.contains("*")) {
        seedsAsClassDiagram.addAll(ontologySeeds.keySet());
      }
      OntologyGraph ontologyGraph =
          new OntologyGraph(
              CollectionConverters.asScala(Collections.emptyMap()),
              CollectionConverters.asScala(Collections.emptySet())
          );
      for (String seedId : seedsAsClassDiagram) { // NOPMD
        if (!ontologySeeds.containsKey(seedId)) {
          mainAppLogger.log(Level.INFO, "Ontology {0} was not found!", seedId);
        } else {
          final OntologySeed seed = ontologySeeds.get(seedId);
          ontologyGraph = ontologyGraph.mergeWith(seed.toOntologyGraph());
          final String dotDescr = ontologyGraph.toClassDiagramWithDot();

          Path dotFile;
          if (outputFormats.contains("dot")) {
            dotFile = Paths.get(outputPath.getAbsolutePath() + ".dot");
          } else {
            String tmpDir = System.getProperty("java.io.tmpdir");
            dotFile = Paths.get(tmpDir, seedId + "_classDiagram.dot");
          }
          try {
            Files.write(dotFile, dotDescr.getBytes(UTF_8));
            for (String format :
                outputFormats.stream().filter(s -> ! s.equals("dot")).collect(
                Collectors.toSet())) {
              Path outFile = Paths.get(outputPath.getAbsolutePath() + "." + format);
              String dotCmd = "dot -T " + format + " -o " + outFile + " " + dotFile;
              Process gvProc = Runtime.getRuntime().exec(dotCmd);
              try { gvProc.waitFor(10, TimeUnit.SECONDS); }
              catch (InterruptedException e) {
                mainAppLogger.log(
                    Level.SEVERE,
                    "While writing '{0}' and converting it to '{1}', the following interruption occurred:\n{2}",
                    new String[]{dotFile.toString(), outFile.toString(), e.getMessage()}
                );
              }
            }
          } catch (IOException e) {
            mainAppLogger.log(
                Level.SEVERE,
                "While writing dot file '{0}', the following error occurred:\n{1}",
                new String[]{dotFile.toString(), e.getMessage()}
            );
          }
        }
      }
    }
  }

  /**
   * Method used to display ontology seeds.
   *
   * @param verbose increase verbosity if {@code true}
   * @param soslFiles collection of files containing the SOS to display
   * @param viewedSeeds the seeds to be displayed.
   */
  public static void viewUsingJGraphx(
      final boolean verbose,
      final Collection<File> soslFiles,
      final Collection<String> viewedSeeds
  ) {
    final Map<String, OntologySeed> ontologySeeds = new HashMap<>();
    for (File soslFile : soslFiles) {
      ontologySeeds.putAll(retrieveSeeds(verbose, soslFile));
    }
    if (viewedSeeds != null) {
      if (viewedSeeds.contains("*")) {
        viewedSeeds.addAll(ontologySeeds.keySet());
      }
      for (String seedId : viewedSeeds) { // NOPMD
        if (!ontologySeeds.containsKey(seedId)) {
          mainAppLogger.log(Level.INFO, "Ontology {0} was not found!", seedId);
        } else {
          final OntologySeed seed = ontologySeeds.get(seedId);
          ForestryGUI.viewOntologyGraph(seed.toMXGraph());
        }
      }
    }
  }

  /**
   * Runs the provided SOS file through the 'grun' tool (see {@link TestRig}).
   *
   * @param withTree whether or not to display the parse tree.
   * @param withGui whether or not to use a GUI to display the parse tree.
   * @param withTokens whether or not to display tokens as interpreted by the lexer.
   * @param withTrace whether or not to display a parsing trace.
   * @param withSLL whether or not to use SLL parsing algorithm.
   * @param withDiagnostics whether or not to
   * @param encoding the specific encoding to use.
   * @param psFile the postscript file to produce.
   * @param soslFile SOSL {@link File} to be checked.
   */
  public static void grun(// NOPMD
      final boolean withTree,
      final boolean withGui,
      final boolean withTokens,
      final boolean withTrace,
      final boolean withSLL,
      final boolean withDiagnostics,
      final String encoding,
      final String psFile,
      final File soslFile
  ) {
    if (!soslFile.canRead()) {
      final String errorMsg = String.format(FILE_S_CAN_NOT_BE_FOUND_OR_READ, soslFile.getPath());
      mainAppOutput.out(errorMsg);
      mainAppLogger.log(Level.SEVERE, errorMsg);
    } else {
      try {
        final SOSLTestRig grun = new SOSLTestRig();
        if (withTree) {
          grun.withTree();
        }
        if (withGui) {
          grun.withGui();
        }
        if (withTokens) {
          grun.withTokens();
        }
        if (withTrace) {
          grun.withTrace();
        }
        if (withSLL) {
          grun.withSLL();
        }
        if (withDiagnostics) {
          grun.withDiagnostics();
        }
        if (encoding != null) {
          grun.withEncoding(encoding);
        }
        if (psFile != null) {
          grun.withPS(psFile);
        }
        grun.process(soslFile);
      } catch (Exception e) { // NOPMD
        mainAppOutput.out(
            "GRUN returned the following error message: "
                + "["
                + e.getClass()
                + "] "
                + e.getMessage());
      }
    }
  }

  /**
   * Runs the provided file through the SOSL parser.
   *
   * @param soslFile SOSL {@link File} to be checked.
   */
  public static void tryToParse(final File soslFile) {
    if (!soslFile.canRead()) {
      final String errorMsg = String.format(FILE_S_CAN_NOT_BE_FOUND_OR_READ, soslFile);
      mainAppOutput.out(errorMsg);
      mainAppLogger.log(Level.SEVERE, errorMsg);
    } else {
      try {
        final SOSLParser parser = ParsingUtils.getParserOfFile(soslFile.toPath());
        ParserRuleContext cst = null; // NOPMD
        try {
          cst = parser.sosFile();
        } catch (Exception e) { // NOPMD
          if (mainAppLogger.isLoggable(Level.SEVERE)) {
            mainAppLogger.severe("Parsing did not went well: [" + e + "] " + e.getMessage());
            mainAppOutput.out("Parsing did not went well. Have a look to the logs.");
          } else {
            mainAppOutput.out("Parsing did not went well: " + e.getMessage());
          }
        }
        final int nbErrors = parser.getNumberOfSyntaxErrors();
        if (nbErrors == 0) {
          mainAppOutput.out("Parsing seems to have gone through.");
        } else {
          mainAppOutput.out();
          mainAppOutput.out("Found " + nbErrors + " syntax error(s).");
          mainAppOutput.out("Parser has reached: " + parser.getCurrentToken());
          if (parser.getState() >= 0) {
            mainAppOutput.out("Parser was expecting: " + parser.getExpectedTokens());
          }
          mainAppOutput.out("Parser's call stack is: " + parser.getRuleInvocationStack());
          final ParserRuleContext ctx = parser.getContext();
          if (ctx == null) {
            mainAppOutput.out("There is no context available.");
          } else {
            mainAppOutput.out("Parser's context is: " + ctx.toInfoString(parser));
            mainAppOutput.out("Parsing context: " + ctx.getText());
          }
        }
        if (cst != null) {
          final Map<String, OntologySeed> sosMap = (new CST2ASTVisitor()).visit(cst); // NOPMD
          mainAppOutput.out("Found the following ontologies!");
          sosMap.forEach(
              (id, sos) -> {
                mainAppOutput.out();
                mainAppOutput.out(sos.toMultilineVerboseString());
              });
        }
      } catch (IOException e) {
        mainAppLogger.log(Level.SEVERE, "Parsing failed with an IOException!", e);
      }
    }
  }

}
